<?php
include_once '../../scripts/bd.php';
session_start();
$now = time();
if(!isset($_SESSION['id-mailer'])){
    header("Location: /account/");
}


if ($now > $_SESSION['expire']) {
    add_log("<span class='badge badge-warning'>Session has expired</span>");
    session_start();
    session_destroy();
    header("Location: /account/");
}else{
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
    $_SESSION['start'] = time();
    $_SESSION['expire'] = $_SESSION['start'] + (60*60);
}


function add_log($str){
    $id_mailer=$_SESSION['id-mailer'];
    $id_server=$_SESSION['id-server'];
    $connect_ip=$_SERVER["REMOTE_ADDR"];
    $activity=addslashes($str);
    $date_activity=date('Y-m-d h:i:s');
    bd::query("INSERT INTO mailer_log VALUES (NULL, '$id_server', '$id_mailer', '$connect_ip', '$activity', '$date_activity');");
}

function get_sponsors_count(){
    $query = bd::query("SELECT COUNT(*) FROM sponsor WHERE active=1");
    $row = mysql_fetch_row($query);
    echo $row[0];
}

function get_servers_count(){
    $query = bd::query("SELECT COUNT(*) FROM server WHERE active=1");
    $row = mysql_fetch_row($query);
    echo $row[0];
}

function get_ips_count(){
    $query = bd::query("SELECT COUNT(*),server.id FROM server_vmta,server WHERE server_vmta.active=1 && server.active=1 && server.id=server_vmta.id_server");
    $row = mysql_fetch_row($query);
    echo $row[0];
}

function get_domains_count(){
    $query = bd::query("SELECT COUNT(DISTINCT server_vmta.host_name),server.id FROM server_vmta,server WHERE host_type='Domain' && server_vmta.active=1 && server.active=1 && server.id=server_vmta.id_server");
    $row = mysql_fetch_row($query);
    echo $row[0];
}

  // echo "Your session has expired! <a href='http://localhost/somefolder/login.php'>Login here</a>";  

   /* $time1=date('Y-m-d h:i:s');
    $time2=date('50');  
    $diff=$time1-$time1;
    
    echo 'Time now: '.$time1.'</br>';
    echo 'Time before: '.$time2.'</br>';
    echo 'Different: '.date('s',$diff);*/
    