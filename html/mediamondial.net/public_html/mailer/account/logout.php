<?php
    include_once 'session.php';
    add_log("<span class='badge badge-important'>Logged out sucessfully</span>");
    session_start();
    session_destroy();
    header("Location: /account/");
?>
