$(document).ready(function(){
    razmessage();
});

function razmessage(){
  $("#message-error").hide();
}

function close_message_error(){
    $("#message-error").hide("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function refresh(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'post',
        data: {
            //id_news: document.getElementById("news").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var html= "<tr class='heading'><td class='cell-icon'></td><td class='cell-title'>Sponsor Name</td><td class='cell-status hidden-phone hidden-tablet'>Status</td><td class='cell-time align-right'>Options</td></tr>";
            var inp1='';
            var inp2='';
            for (var i = 0; i < list.length; i++) {
                switch (list[i].platform){
                    case 'HitPath': inp1='username';inp2='password';break;
                    case 'Cake': inp1='u';inp2='p';break;
                    default : inp1='xxx';inp2='xxx';break;
                }
                html+="<tr class='task'><td class='cell-icon'><i class='icon-checker high'></i></td><td class='cell-title'><div>"+list[i].name+"</div></td><td class='cell-status hidden-phone hidden-tablet'><b class=''>Active</b></td><td class='cell-time align-right'><form id='' action='"+list[i].login_page+"' method='POST' target='_blank'><input type='hidden' id='username' name='"+inp1+"' value='"+list[i].login+"' /><input type='hidden' id='password' name='"+inp2+"' value='"+list[i].password+"' /><input class='btn btn-primary' type='submit' value='Go'/></form></td></tr>";
            }
            $("#show").html(html);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}