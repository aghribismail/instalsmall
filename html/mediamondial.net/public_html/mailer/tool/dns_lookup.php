﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tools | DNS LookUp</title>
    <link type="text/css" href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="/static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {

        } );
    </script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php include("../inc.top_menu.php");  ?>
			</div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <?php include("../left_menu.html");  ?>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>DNS LookUp <div class="processing" id="processing"></div></h3></div>
                            
                            <div class="module-body">
                                <div class="alert" id="message-warning">
                                    <button type="button" class="close" onclick="close_message_warning();">×</button>
                                    <strong>Warning!</strong> Please verify your data
                                </div>
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Hostname or IP</label>
                                        <div class="controls">
                                            <input type="text" id="host" name="host" placeholder="" class="span5">
                                            <select tabindex="1" data-placeholder="Select here.." id="dns_type" name="dns_type" class="span2">
                                                <option value="ALL">ALL</option>
                                                <option value="A">A</option>
                                                <option value="AAAA">AAAA (IPv6)</option>
                                                <option value="MX">MX</option>
                                                <option value="NS">NS</option>
                                                <option value="SOA">SOA</option>
                                                <option value="TXT">TXT</option>
                                            </select>
                                            <button class="btn btn-primary" type="button" onclick="lookup();"><i class="icon-search icon-white"></i>&nbsp;LookUp</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="pmta_report">
                            </div>
                            
                        </div>
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>