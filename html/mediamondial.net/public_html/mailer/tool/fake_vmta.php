﻿<!DOCTYPE html>
<html lang="en">
<?php 
    
    //include_once '../account/session.php';
    //include_once '../../scripts/bd.php';
    
    
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tools | Fake VMTA</title>
    <link type="text/css" href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="/static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="fake_vmta.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {

        } );
    </script>
</head>
<body>
	 <div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<?php include("../inc.top_menu.php");  ?>
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <?php include("../left_menu.html");  ?>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Fake VMTA <div class="processing" id="processing"></div></h3></div>
                            
                            <div class="module-body">
                                <div class="alert" id="message-warning">
                                    <button type="button" class="close" onclick="close_message_warning();">×</button>
                                    <strong>Warning!</strong> Please verify your data
                                </div>
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <div class="form-horizontal row-fluid">
                                    
                                    
                                     <!-- Servers -->
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Server</label>
                                        <div class="controls">
                                             <select tabindex="1" data-placeholder="Select here.." id="servers" name="servers" class="span5" onchange="loadVMTAs();">
                                                <option value="0">Select here...</option>
                                                <?php
                                                    include_once('/static/scripts/bdd.php');
                                                    $sqlGetServers          =   
                                                    "
                                                        SELECT  S.id,S.name
                                                        FROM    server S
                                                        WHERE   S.active    =   ?
                                                        AND     id          =   ?
                                                    ";
                                                    $cmdGetServers          =   $bdd->prepare($sqlGetServers);
                                                    $cmdGetServers->execute(array(1,$id_server));
                                                    while($servers          =   $cmdGetServers->fetch())
                                                    {
                                                        echo '<option value="'.$servers["id"].'">'.$servers["name"].'</option>';
                                                    }
                                                    $cmdGetServers->closeCursor();
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                    <!-- IPs -->
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">IPs</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="ips"  name="ips" class="span5">
                                                <option value="0">Select here...</option>
                                                 <?php
                                                    include('/static/scripts/bdd.php');
                                                    $sqlGetIPs          =   
                                                    "
                                                        select distinct(I.address_ip),I.id from server_vmta I where I.id_server=? and I.active=?
                                                    ";
                                                    $cmdGetIPs          =   $bdd->prepare($sqlGetIPs);
                                                    $cmdGetIPs->execute(array($id_server,1));
                                                    while($IPs          =   $cmdGetIPs->fetch())
                                                    {
                                                       echo '<option value="'.$IPs["id"].'">'.$IPs["address_ip"].'</option>';
                                                    }
                                                    $cmdGetIPs->closeCursor();
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    <!-- Domain -->
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Domain</label>
                                        <div class="controls">
                                            <input type="text" id="fake_domaine" name="fake_domaine" placeholder="" class="span5">
                                        </div>
                                    </div>
                                
                                     <!-- BTN Submit -->
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput"></label>
                                        <div class="controls">
                                            <button class="btn btn-primary" id="btn_create_fake_vmta" name="btn_create_fake_vmta" type="button"><i class="icon-plus icon-white"></i>&nbsp;Create Fake VMTA</button>
                                        </div>
                                    </div>
                                
                                
                                </div>
                            </div>
                            
                            
                        </div>
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>
