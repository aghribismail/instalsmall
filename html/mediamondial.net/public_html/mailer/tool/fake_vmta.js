function razmessage()
{
    $("#message-error").hide();
    $("#message-warning").hide();
}

function close_message_error()
{
    $("#message-error").hide("slow");
}

function close_message_warning()
{
    $("#message-warning").slideUp("slow");
}

function processing_show()
{
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide()
{
    $("#processing").hide();
}

function validDomain($domain)
{
        if(stripos($domain, 'http://') === 0)
    {
        $domain = substr($domain, 7);
    }

    ///Not even a single . this will eliminate things like abcd, since http://abcd is reported valid
    if(!substr_count($domain, '.'))
    {
        return false;
    }

    if(stripos($domain, 'www.') === 0)
    {
        $domain = substr($domain, 4);
    }

    $again = 'http://' . $domain;
    if(!filter_var ($again, FILTER_VALIDATE_URL))
        return false;
    else
        return true;
}




function check_data()
{
    if(document.getElementById("servers").value===0)
        return false;
    
    if(document.getElementById("ips").value===0)
        return false;
    
    if(document.getElementById("fake_domaine").value==="")
        return false;
    
    /*
    $domain =   $("#fake_domaine").val();
    if(!validDomain($domain))
        return false;
    */
    return true;
}



$(document).ready(function()
{
    //Hide Alerts :
    razmessage();
    
    
    //Combo Server : Change
    /*
    $('#servers').change(function()
    {
        //Get the is of the selected server :
        var id_server	=   $('#servers option:selected').val();
        if(id_server>0)
        {
            processing_show();
            $.get
            (
                './scripts.php',
                {id_server : id_server,action : 'get_ips'},
                function(data)
                {
                    //Delete Combo IPs :
                    $("#ips").empty();
                    
                    //Load Combo IPs :
                    $("#ips").append(data);
                   
                    processing_hide();
                }
            );
        }
    });
    */
    
    //Bouton Create Fake VMTA :
    $('#btn_create_fake_vmta').click(function()
    {
	var id_server    =   $('#servers').val();
        var id_ip        =   $('#ips').val();
        var fake_domain  =   $('#fake_domaine').val();
       
       
        //1-Validation :
        if(check_data())
        {
            //2-Creation VMTA :
            processing_show();
            $.get
            (
                './scripts.php',
                {
                    id_server   :   id_server,
                    id_ip       :   id_ip,
                    fake_domain :   fake_domain,
                    action      :   'create_fake_vmta'
                },
                function(data)
                {
                    alert(data);
                    processing_hide();
                }
            );
        }
        else
        {
            $("#message-warning").slideDown("slow");
            setTimeout(function(){close_message_warning();},3000);
        }
    });
});
