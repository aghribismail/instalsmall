$(document).ready(function(){
    razmessage();
});

function razmessage(){
    $("#message-error").hide();
    $("#message-warning").hide();
}

function close_message_error(){
    $("#message-error").hide("slow");
}

function close_message_warning(){
    $("#message-warning").slideUp("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function check_data(){
    if(document.getElementById("isp").value==="0")return false;
    if(document.getElementById("email_address").value==="")return false;
    if(document.getElementById("password").value==="")return false;
    if(document.getElementById("folder").value==="0")return false;
    if(!document.getElementById('from').checked && !document.getElementById('subject').checked && !document.getElementById('body').checked){
        return false;
    }
    
    return true;
}

function check_data_2(){
    if(document.getElementById("host").value==="")return false;
    return true;
}

function fetch_data(){
    if(check_data()){
        processing_show();
        var data_retrieved="";
        if(document.getElementById('from').checked){
            data_retrieved="from";
        }
        if(document.getElementById('subject').checked){
            data_retrieved="subject";
        }
        if(document.getElementById('body').checked){
            data_retrieved="body";
        }
        $.ajax({
            
            url: 'scripts.php?action=fetch_data',
            type: 'post',
            data: {
                isp: document.getElementById("isp").value,
                email_address: document.getElementById("email_address").value,
                password: document.getElementById("password").value,
                folder: document.getElementById("folder").value,
                data_retrieved: data_retrieved
            },
            success: function (data) {
                //var str=data.replace(/(\r\n|\n|\r)/gm,"");
                var str=data;
                $("#pmta_report").html(str);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                setTimeout(function(){close_message_error();},3000);
                processing_hide();
            }
        });
    }else{
        $("#message-warning").slideDown("slow");
        setTimeout(function(){close_message_warning();},3000);
    }
}

function lookup(){
    if(check_data_2()){
        processing_show();
        $.ajax({
            url: 'scripts.php?action=lookup',
            type: 'post',
            data: {
                host: document.getElementById("host").value,
                dns_type: document.getElementById("dns_type").value
            },
            success: function (data) {
                //var str=data.replace(/(\r\n|\n|\r)/gm,"");
                var str=data;
                $("#pmta_report").html(str);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                setTimeout(function(){close_message_error();},3000);
                processing_hide();
            }
        });
    }else{
        $("#message-warning").slideDown("slow");
        setTimeout(function(){close_message_warning();},3000);
    }
}

function show_isps(){
    var html= "<option value='0' disabled=''>Waiting for extracting ISPs...</option>";
    $("#isp").html(html);
    $.ajax({
        url: 'scripts.php?action=get_isps',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#isp").html(html);    
        }
    });
}






function add(){
    if(check_data()){
        processing_show();
        $.ajax({
            url: 'scripts.php?action=add',
            type: 'post',
            data: {
                name: document.getElementById("name").value,
                extensions : document.getElementById("extensions").value,
                hard_bounce_code : document.getElementById("hard_bounce_code").value
            },
            success: function(data) {
                var str=data.replace(/(\r\n|\n|\r)/gm,"");
                if(str==="0"){ 
                    $("#message-success").slideDown("slow");
                    setTimeout(function(){close_message_success();},3000);
                    clear_data(); 
                }
                if(str==="1"){ 
                    $("#message-custom").text("ISP Name already in use");
                    $("#message-warning-custom").slideDown("slow");
                    setTimeout(function(){close_message_warning_custom();},3000);
                }
                if(str==="2"){ 
                    $("#message-error").slideDown("slow");
                    setTimeout(function(){close_message_error();},3000);
                }
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                setTimeout(function(){close_message_error();},3000);
                processing_hide();
            }
            
        }); 
    }else{
        $("#message-warning").slideDown("slow");
        setTimeout(function(){close_message_warning();},3000);
    }
}

/*function delete_queue_command(){
    processing_show();
    $.ajax({
            url: 'scripts.php?action=delete_queue_command',
            type: 'get',
            data: {
                isp: document.getElementById("isp").value,
                vmta: document.getElementById("vmta").value
            },
            success: function (data) {
                $("#pmta_report").html(data);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                processing_hide();
            }
    });
}*/