$(document).ready(function(){
    razmessage();
});

function razmessage(){
  $("#message-warning").hide();
  $("#message-warning-custom").hide();
  $("#message-error").hide();
  $("#message-success").hide();
}

function close_message_warning(){
    $("#message-warning").slideUp("slow");
}

function close_message_warning_custom(){
    $("#message-warning-custom").slideUp("slow");
}

function close_message_error(){
    $("#message-error").slideUp("slow");
}

function close_message_success(){
    $("#message-success").slideUp("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function today(){
    var today=new Date();
    $("#from_mm").val(today.getMonth()+1);
    $("#from_dd").val(today.getDate());
    $("#from_yy").val(today.getFullYear());

    $("#to_mm").val(today.getMonth()+1);
    $("#to_dd").val(today.getDate());
    $("#to_yy").val(today.getFullYear());
}

function yesterday(){
    var today=new Date();
    $("#from_mm").val(today.getMonth()+1);
    $("#from_dd").val(today.getDate()-1);
    $("#from_yy").val(today.getFullYear());

    $("#to_mm").val(today.getMonth()+1);
    $("#to_dd").val(today.getDate()-1);
    $("#to_yy").val(today.getFullYear());
}


function refresh_stats_data_news(id_data_news){
    refresh_data_news_opens(id_data_news);
    refresh_data_news_clicks(id_data_news);
    refresh_data_news_bounces(id_data_news);
    refresh_data_news_earnings(id_data_news);
}

function refresh_data_news_opens(id_data_news){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_data_news_opens',
        type: 'get',
        data: {
            id_data_news: id_data_news,
            date_from: document.getElementById('from_yy').value+'-'+document.getElementById('from_mm').value+'-'+document.getElementById('from_dd').value,
            date_to: document.getElementById('to_yy').value+'-'+document.getElementById('to_mm').value+'-'+document.getElementById('to_dd').value
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-opens").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_data_news_clicks(id_data_news){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_data_news_clicks',
        type: 'get',
        data: {
            id_data_news: id_data_news,
            date_from: document.getElementById('from_yy').value+'-'+document.getElementById('from_mm').value+'-'+document.getElementById('from_dd').value,
            date_to: document.getElementById('to_yy').value+'-'+document.getElementById('to_mm').value+'-'+document.getElementById('to_dd').value
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_data_news_bounces(id_data_news){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_data_news_bounces',
        type: 'get',
        data: {
            id_data_news: id_data_news,
            date_from: document.getElementById('from_yy').value+'-'+document.getElementById('from_mm').value+'-'+document.getElementById('from_dd').value,
            date_to: document.getElementById('to_yy').value+'-'+document.getElementById('to_mm').value+'-'+document.getElementById('to_dd').value
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-bounces").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_data_news_earnings(id_data_news){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_data_news_earnings',
        type: 'get',
        data: {
            id_data_news: id_data_news,
            date_from: document.getElementById('from_yy').value+'-'+document.getElementById('from_mm').value+'-'+document.getElementById('from_dd').value,
            date_to: document.getElementById('to_yy').value+'-'+document.getElementById('to_mm').value+'-'+document.getElementById('to_dd').value
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-earnings").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

/*
function refresh_campaign_bounces(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_bounces',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-bounces").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_bounces_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_bounces_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#bounces-rate").text(str);
            $("#bounces-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerpage(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerpage',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks-offerpage").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerpage_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerpage_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#clicks-offerpage-rate").text(str);
            $("#clicks-offerpage-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerunsub(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerunsub',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks-offerunsub").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerunsub_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerunsub_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#clicks-offerunsub-rate").text(str);
            $("#clicks-offerunsub-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_serverunsub(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_serverunsub',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks-serverunsub").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_serverunsub_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_serverunsub_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#clicks-serverunsub-rate").text(str);
            $("#clicks-serverunsub-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}
*/
function lisibilite_nombre(nbr){
    var nombre = ''+nbr;
    var retour = '';
    var count=0;
    for(var i=nombre.length-1 ; i>=0 ; i--){
            if(count!=0 && count % 3 == 0)
                    retour = nombre[i]+' '+retour ;
            else
                    retour = nombre[i]+retour ;
            count++;
    }
    return retour;
}



