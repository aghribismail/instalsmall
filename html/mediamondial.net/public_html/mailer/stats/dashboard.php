﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link type="text/css" href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="/static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            
            today();
            
            refresh_stats_data_news(<?php echo $_GET["id_news"];?>);
        });
    </script>
</head>
<body>
     <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php include("../inc.top_menu.php");  ?>
			</div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <?php include("../left_menu.html");  ?>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                       <div class="alert alert-error" id="message-error">
                            <button type="button" class="close" onclick="close_message_error();">×</button>
                            <strong>Error!</strong> There was an error while executing your request
                        </div>
                        
                        <div class="btn-controls">
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-list-alt"></i><b id="total-selected">-</b><p class="text-muted">Selected</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-share"></i><b id="total-processed">-</b><p class="text-muted">Processed</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-check"></i><b id="total-delivered">-</b><p class="text-muted">Delivered</p></a>
                            </div>
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-remove-circle"></i><b id="total-bounces">-</b><p class="text-muted">Total Bounce</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-ban-circle"></i><b id="total-hardbounce">-</b><p class="text-muted">Hard Bounce</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-warning-sign"></i><b id="total-softbounce">-</b><p class="text-muted">Soft Bounce</p></a>
                            </div>
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-folder-open"></i><b id="total-opens">-</b><p class="text-muted">Opens</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-hand-up"></i><b id="total-clicks">-</b><p class="text-muted">Clicks</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-money"></i><b id="total-earnings">-</b><p class="text-muted">Earnings</p></a>
                            </div>
                        </div>
                        <div class="btn-controls">
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-warning-sign"></i><b id="total-bounces">-</b><p class="text-muted">Hard Bounces</p></a>
                            </div>
                        </div>
                        <!--/#btn-controls-->
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>