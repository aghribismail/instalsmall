<?php
    include_once '../../account/session.php';
    include_once '../../scripts/bd.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $id_mailer=$_SESSION['id-mailer'];
   
    if(isset($_GET['action'])){
     $action=$_GET['action'];
     if($action=='get_data_news_opens')get_data_news_opens();
     if($action=='get_data_news_clicks')get_data_news_clicks();
     if($action=='get_data_news_bounces')get_data_news_bounces();
     if($action=='get_data_news_earnings')get_data_news_earnings();
     /*if($action=='get_campaign_opens_rate')get_campaign_opens_rate();
     if($action=='get_campaign_clicks')get_campaign_clicks();
     if($action=='get_campaign_clicks_rate')get_campaign_clicks_rate();
     if($action=='get_campaign_bounces')get_campaign_bounces();
     if($action=='get_campaign_bounces_rate')get_campaign_bounces_rate();
     if($action=='get_campaign_clicks_offerpage')get_campaign_clicks_offerpage();
     if($action=='get_campaign_clicks_offerpage_rate')get_campaign_clicks_offerpage_rate();
     if($action=='get_campaign_clicks_offerunsub')get_campaign_clicks_offerunsub();
     if($action=='get_campaign_clicks_offerunsub_rate')get_campaign_clicks_offerunsub_rate();
     if($action=='get_campaign_clicks_serverunsub')get_campaign_clicks_serverunsub();
     if($action=='get_campaign_clicks_serverunsub_rate')get_campaign_clicks_serverunsub_rate();*/
    }
   
    function get_data_news_opens(){
        $opens="-";
        if($_SESSION['id-news']==$_GET["id_data_news"]){
            $query=bd::query("SELECT COUNT(track_open.id), data_list.id, data_list.id_news FROM track_open,data_list WHERE track_open.id_list=data_list.id && data_list.id_news='{$_GET["id_data_news"]}'  && (track_open.date_open BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $opens=$row[0];
        }
        
        echo $opens;
    }
   
    function get_data_news_clicks(){
        $clicks="-";
        if($_SESSION['id-news']==$_GET["id_data_news"]){
            $query=bd::query("SELECT COUNT(track_click.id), data_list.id, data_list.id_news FROM track_click,data_list WHERE track_click.id_list=data_list.id && data_list.id_news='{$_GET["id_data_news"]}'  && (track_click.date_click BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $clicks=$row[0];
        }
        echo $clicks;
    }
   
    function get_data_news_bounces(){
        $bounces="-";
        if($_SESSION['id-news']==$_GET["id_data_news"]){
            $query=bd::query("SELECT COUNT(track_hard_bounce.id), data_list.id, data_list.id_news FROM track_hard_bounce,data_list WHERE track_hard_bounce.id_list=data_list.id && data_list.id_news='{$_GET["id_data_news"]}'  && (track_hard_bounce.date_bounce BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $bounces=$row[0];
        }
        echo $bounces;
    }
    
    function get_data_news_earnings(){
        $earnings="-";
        if($_SESSION['id-news']==$_GET["id_data_news"]){
            $query=bd::query("SELECT SUM(track_earning.amount), data_list.id, data_list.id_news FROM track_earning,data_list WHERE track_earning.id_list=data_list.id && data_list.id_news='{$_GET["id_data_news"]}'  && (track_earning.date_earning BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $earnings=$row[0];
            if($row[0]==null){
                $earnings="0";
            }
        }
        echo $earnings;
    }
    
    function get_data_news_name($id){
        $query = bd::query("SELECT name FROM data_news WHERE id='$id'");
        $row=mysql_fetch_array($query);
        echo $row['name'];
    }
   
   
   /*
   
   function get_campaign_opens_rate(){
       $opens_rate="0";
       $query=bd::query("SELECT open_tracker FROM campaign WHERE id='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_array($query);
       if($row['open_tracker']=="1"){
           $query=bd::query("SELECT COUNT(id) FROM track_open WHERE id_campaign='{$_GET["id_campaign"]}'");
           $row=mysql_fetch_row($query);
           $opens=$row[0];
           $query=bd::query("SELECT data_sent FROM campaign WHERE id='{$_GET["id_campaign"]}'");
           $row=mysql_fetch_row($query);
           $sent=$row[0];
           $opens_rate=round(($opens/$sent)*100);
           //$opens_rate.="%";
       }else{
           $opens_rate="-";
       }
       echo $opens_rate;
   }
   
   function get_campaign_clicks(){
       $clicks="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       echo $clicks;
   }
   
   function get_campaign_clicks_rate(){
       $clicks_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT data_sent FROM campaign WHERE id='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $sent=$row[0];
       $clicks_rate=round(($clicks/$sent)*100);
       $clicks_rate.="%";
       echo $clicks_rate;
   }
   
   function get_campaign_bounces(){
       $bounces="0";
       $query=bd::query("SELECT COUNT(id) FROM track_hard_bounce WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $bounces=$row[0];
       echo $bounces;
   }
   
   function get_campaign_bounces_rate(){
       $bounces_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_hard_bounce WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $bounces=$row[0];
       $query=bd::query("SELECT data_sent FROM campaign WHERE id='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $sent=$row[0];
       $bounces_rate=round(($bounces/$sent)*100);
       $bounces_rate.="%";
       echo $bounces_rate;
   }
   
   function get_campaign_clicks_offerpage(){
       $clicks_offerpage="0";
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[LandingPage]'");
       $row=mysql_fetch_row($query);
       $clicks_offerpage=$row[0];
       echo $clicks_offerpage;
   }
   
   function get_campaign_clicks_offerpage_rate(){
       $clicks_offerpage_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[LandingPage]'");
       $row=mysql_fetch_row($query);
       $clicks_offerpage=$row[0];
       $clicks_offerpage_rate=round(($clicks_offerpage/$clicks)*100);
       $clicks_offerpage_rate.="%";
       echo $clicks_offerpage_rate;
   }
   
   function get_campaign_clicks_offerunsub(){
       $clicks_offerunsub="0";
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[Unsubscribe]'");
       $row=mysql_fetch_row($query);
       $clicks_offerunsub=$row[0];
       echo $clicks_offerunsub;
   }
   
   function get_campaign_clicks_offerunsub_rate(){
       $clicks_offerunsub_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[Unsubscribe]'");
       $row=mysql_fetch_row($query);
       $clicks_offerunsub=$row[0];
       $clicks_offerunsub_rate=round(($clicks_offerunsub/$clicks)*100);
       $clicks_offerunsub_rate.="%";
       echo $clicks_offerunsub_rate;
   }
   
   function get_campaign_clicks_serverunsub(){
       $clicks_serverunsub="0";
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[UnsubServer]'");
       $row=mysql_fetch_row($query);
       $clicks_serverunsub=$row[0];
       echo $clicks_serverunsub;
   }
   
   function get_campaign_clicks_serverunsub_rate(){
       $clicks_serverunsub_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[UnsubServer]'");
       $row=mysql_fetch_row($query);
       $clicks_serverunsub=$row[0];
       $clicks_serverunsub_rate=round(($clicks_serverunsub/$clicks)*100);
       $clicks_serverunsub_rate.="%";
       echo $clicks_serverunsub_rate;
   }
   */
   
