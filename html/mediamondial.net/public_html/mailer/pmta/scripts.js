$(document).ready(function(){
    razmessage();
    $("#command").keyup(function(event){
        if(event.keyCode == 13){
              run_command();
        }
    });
});

function razmessage(){
  $("#message-error").hide();
}

function close_message_error(){
    $("#message-error").hide("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function delete_queue_command(){
    processing_show();
    $.ajax({
            url: 'scripts.php?action=delete_queue_command',
            type: 'get',
            data: {
                isp: document.getElementById("isp").value,
                vmta: document.getElementById("vmta").value
            },
            success: function (data) {
                $("#pmta_report").html(data);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                processing_hide();
            }
    });
}

function delete_jobid_command(){
    processing_show();
    $.ajax({
            url: 'scripts.php?action=delete_jobid_command',
            type: 'get',
            data: {
                jobid: document.getElementById("jobid").value
            },
            success: function (data) {
                $("#pmta_report").html(data);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                processing_hide();
            }
    });
}

function run_command(){
    processing_show();
    $.ajax({
            url: 'scripts.php?action=run_command',
            type: 'get',
            data: {
                command: document.getElementById("command").value
            },
            success: function (data) {
                $("#pmta_report").html(data);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                processing_hide();
            }
    });
}

function restart_command(){
    processing_show();
    $.ajax({
            url: 'scripts.php?action=restart_command',
            type: 'get',
            success: function (data) {
                $("#pmta_report").html(data);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                processing_hide();
            }
    });
}

function reset_command(){
    processing_show();
    $.ajax({
            url: 'scripts.php?action=reset_command',
            type: 'get',
            success: function (data) {
                $("#pmta_report").html(data);
                $("#pmta_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                processing_hide();
            }
    });
}

function run_log(){
    processing_show();
    $.ajax({
            url: 'scripts.php?action=run_log',
            type: 'get',
            data: {
                log_type: document.getElementById("log_type").value,
                rows_count: document.getElementById("rows_count").value
            },
            success: function (data) {
                $("#pmta_log_report").html(data);
                $("#pmta_log_report").slideDown("slow");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                processing_hide();
            }
    });
}