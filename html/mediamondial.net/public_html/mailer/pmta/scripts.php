<?php
   include_once '../account/session.php';
   include_once '../../scripts/bd.php';
   
   if(isset($_GET['action'])){
        $action=$_GET['action'];
        if($action=='delete_queue_command')delete_queue_command();
        if($action=='delete_jobid_command')delete_jobid_command();
        if($action=='run_command')run_command();
        if($action=='restart_command')restart_command();
        if($action=='reset_command')reset_command();
        if($action=='run_log')run_log();
   }
   
   function delete_queue_command(){
        $commandglobal="delete --queue=".$_GET["isp"]."/".$_GET["vmta"];
        echo '<pre>';
        if(!empty($commandglobal)){
            $commandglobal=trim($commandglobal);
            $command="sudo pmta ".$commandglobal;
            exec($command,$output,$report);
            if($report=='0'){
                foreach ($output as $value) {
                    echo $value.'<br>';
                }
            }else{
                echo "<div class='text-error'>Unknown command (".$commandglobal.")! use --help for help</div>";
            }
            
        }else{
            echo "<div class='text-warning'>No command specified! use --help for help</br>";
        }
        echo '</pre>';
    }
    
    function delete_jobid_command(){
        $commandglobal="delete --jobId=".$_GET["jobid"];
        echo '<pre>';
        if(!empty($commandglobal)){
            $commandglobal=trim($commandglobal);
            $command="sudo pmta ".$commandglobal;
            exec($command,$output,$report);
            if($report=='0'){
                foreach ($output as $value) {
                    echo $value.'<br>';
                }
            }else{
                echo "<div class='text-error'>Unknown command (".$commandglobal.")! use --help for help</div>";
            }
            
        }else{
            echo "<div class='text-warning'>No command specified! use --help for help</br>";
        }
        echo '</pre>';
    }
   
    function run_command(){
        $commandglobal=$_GET["command"];
        echo '<pre>';
        if(!empty($commandglobal)){
            $command=trim($commandglobal);
            if($command=="restart" || $command=="status"){
                $command="sudo /etc/init.d/pmta ".$commandglobal;
            }else{
                $command="sudo pmta ".$commandglobal;
            }
            exec($command,$output,$report);
            if($report=='0'){
                foreach ($output as $value) {
                    echo $value.'<br>';
                }
            }else{
                echo "<div class='text-error'>Unknown command (".$commandglobal.")! use --help for help</div>";
            }

        }else{
            echo "<div class='text-warning'>No command specified! use --help for help</br>";
        }
        echo '</pre>';
    }
    
    function restart_command(){
        $commandglobal="restart";
        echo '<pre>';
        if(!empty($commandglobal)){
            $commandglobal=trim($commandglobal);
            $command="sudo /etc/init.d/pmta ".$commandglobal;
            exec($command,$output,$report);
            if($report=='0'){
                foreach ($output as $value) {
                    echo $value.'<br>';
                }
            }else{
                echo "<div class='text-error'>Unknown command (".$commandglobal.")! use --help for help</div>";
            }
            
        }else{
            echo "<div class='text-warning'>No command specified! use --help for help</br>";
        }
        echo '</pre>';
    }
    
    function reset_command(){
        $commandglobal="reset counters";
        echo '<pre>';
        if(!empty($commandglobal)){
            $commandglobal=trim($commandglobal);
            $command="sudo pmta ".$commandglobal;
            exec($command,$output,$report);
            if($report=='0'){
                foreach ($output as $value) {
                    echo $value.'<br>';
                }
            }else{
                echo "<div class='text-error'>Unknown command (".$commandglobal.")! use --help for help</div>";
            }
            
        }else{
            echo "<div class='text-warning'>No command specified! use --help for help</br>";
        }
        echo '</pre>';
    }
    
    function run_log(){
        $log_type=$_GET["log_type"];
        $rows_count=$_GET["rows_count"];
        echo '<pre>';
        foreach (new DirectoryIterator('/var/log/pmta/') as $file) {
            if ($file->isFile()){
               if(preg_match('#acct-#i', $file)){
                    ini_set("memory_limit","10000M");
                    $table = explode("\n", file_get_contents($file->getFileInfo()));
                    $cpt=0;
                    $table=  array_reverse($table);
                    foreach ($table as $row) {
                        $cpt++;
                        $str=explode('"', $row);
                        
                        if(count($str)==3){
                            $row=  str_replace(',','|',$str[0]).$str[1].str_replace(',','|',$str[2]);
                        }
                        if(count($str)==5){
                            $row=  str_replace(',','|',$str[0]).$str[1].str_replace(',','|',$str[2]).$str[3].str_replace(',','|',$str[4]);
                        }
                        
                        
                        $fields=explode('|', $row);
                        //$row=$str[0].$str[2].$str[3].$str[4];
                        //$row=  str_replace('"','',$row);
                        
                        
                        //$recipient=$fields[7];
                        $smtp_message=explode(';',$fields[9]);
                        $smtp_message=$smtp_message[1];
						
						$vmta=$fields[19];
                        $ip_address=$fields[16];
                        $id_campaign=$fields[20];
                        /*if($id_campaign!='' && $id_campaign!='0'){
                            $recipient=explode('@',$recipient);
                            $recipient=substr($recipient[0],0,1).'**********'.substr($recipient[0],-1).'@'.$recipient[1];
                        }*/
                        
                        if($log_type=='a'){
                            if($fields[0]=='d'){
                                echo "<div class='text-success'>Delivered</div>";
                                echo 'VMTA: '.$vmta."\n";
                                echo 'IP Address: '.$ip_address."\n";
                                echo 'SMTP Infos: '.$smtp_message."\n";
                                echo '-----------------------------------------------------------------------------------';
                            }
                            if($fields[0]=='b'){
                                echo "<div class='text-error'>Bounced</div>";
                                echo 'VMTA: '.$vmta."\n";
                                echo 'IP Address: '.$ip_address."\n";
                                echo 'SMTP Infos: '.$smtp_message."\n";
                                echo '-----------------------------------------------------------------------------------';
                            }
                            
                        }
                        
                        if($log_type=='d'){
                            if($fields[0]=='d'){
                                echo "<div class='text-success'>Delivered</div>";
                                echo 'VMTA: '.$vmta."\n";
                                echo 'IP Address: '.$ip_address."\n";
                                echo 'SMTP Infos: '.$smtp_message."\n";
                                echo '-----------------------------------------------------------------------------------';
                            }
                        }
                        
                        if($log_type=='b'){
                            if($fields[0]=='b'){
                                echo "<div class='text-error'>Bounced</div>";
                                echo 'VMTA: '.$vmta."\n";
                                echo 'IP Address: '.$ip_address."\n";
                                echo 'SMTP Infos: '.$smtp_message."\n";
                                echo '-----------------------------------------------------------------------------------';
                            }
                        }
                         
                        if($cpt==$rows_count)break;
                       
                    }
                }
            }
        }
        echo '</pre>';
    }
