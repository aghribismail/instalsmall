$(document).ready(function(){
    razmessage();
});

function razmessage(){
  $("#message-error").hide();
}

function close_message_error(){
    $("#message-error").hide("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function refresh(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            $('#show').dataTable().fnClearTable();
            for (var i = 0; i < list.length; i++) {
                $('#show').dataTable().fnAddData([ 
                    list[i].id,
                    list[i].name,
                    list[i].provider,
                    "<span class='badge badge-success'>Active</span>",
                    "<i class='icon-link icon-white' title='Go' style='cursor: pointer;' onclick="+"window.open('http://"+list[i].main_ip+"/app/mailer','_blank')"+"></i>&nbsp;&nbsp;<i class='icon-tasks icon-white' title='VMTAs' style='cursor: pointer;' onclick="+"location.href='vmta/show.php?id_server="+list[i].id+"'"+"></i>" 
                ]);
            }
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
            
        }
    });
}