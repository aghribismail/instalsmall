$(document).ready(function(){
    razmessage();
});

function razmessage(){
  $("#message-warning").hide();
  $("#message-warning-custom").hide();
  $("#message-error").hide();
  $("#message-success").hide();
}

function close_message_warning(){
    $("#message-warning").slideUp("slow");
}

function close_message_warning_custom(){
    $("#message-warning-custom").slideUp("slow");
}

function close_message_error(){
    $("#message-error").slideUp("slow");
}

function close_message_success(){
    $("#message-success").slideUp("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function check_data(){
   if(document.getElementById("name").value==="")return false;
   if($('#address_ip option:selected').length===0)return false;
   if(document.getElementById("host_name").value==="")return false;
   if (!/^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/.test(document.getElementById("host_name").value)){
	  return false;
   }
   return true;
}

function clear_data(){
   document.getElementById("name").value="";
   document.getElementById("address_ip").value="0";
   document.getElementById("host_name").value="";
   ip_select_count();
}

function add(){
	var address_ips='';
	$('#address_ip option:selected').each(function() {
		address_ips+=$(this).val()+';';
	});
	address_ips=address_ips.substring(0, address_ips.length - 1);

    if(check_data()){
        processing_show();
        $.ajax({
            url: 'scripts.php?action=add',
            type: 'post',
            data: {
                name: document.getElementById("name").value,
                address_ips: address_ips,
                host_name: document.getElementById("host_name").value
            },
            success: function(data) {
                var str=data.replace(/(\r\n|\n|\r)/gm,"");
				var ok=true;
                if(str==="0"){ 
                    $("#message-success").slideDown("slow");
                    setTimeout(function(){close_message_success();},3000);
                    clear_data();
					ok=false;
                }
                if(str==="1"){ 
                    $("#message-custom").text("VMTA Name already in use for this Server");
                    $("#message-warning-custom").slideDown("slow");
                    setTimeout(function(){close_message_warning_custom();},3000);
					ok=false;
                }

				if(ok){
					$("#message-custom").text(str);
					$("#message-warning-custom").slideDown("slow");
					setTimeout(function(){close_message_warning_custom();},3000);
				}


                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                setTimeout(function(){close_message_error();},3000);
                processing_hide();
            }
            
        });
    }else{
        $("#message-warning").slideDown("slow");
        setTimeout(function(){close_message_warning();},3000);
    }
}

function generate_name(){
	var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyz";
    for( var i=0; i < 5; i++ )text += possible.charAt(Math.floor(Math.random() * possible.length));
	$("#name").val('mta'+text);
}

function refresh(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        data: {
            id_server: document.getElementById("server").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            $('#show').dataTable().fnClearTable();
            var status='';
			var fake="";
            for (var i = 0; i < list.length; i++) {
                var params=list[i].id_vmta+","+list[i].provider+","+list[i].block_ip+","+list[i].address_ip;
				if(list[i].host_type==1)fake="  <span class='label label-default'>Fake</span>";
                $('#show').dataTable().fnAddData([
                    list[i].id_vmta,
                    list[i].name+fake,
                    list[i].address_ip,
                    list[i].host_name,
                    list[i].provider,
                    list[i].server_name,
                    "<div class='center process-api' id='idv-status"+list[i].id_vmta+"'><span class='badge badge-success'>Active</span></div>",
                    "<i class='icon-refresh icon-white' title='Refresh' style='cursor: pointer;' onclick='get_status("+'"'+params+'"'+");'></i>&nbsp;&nbsp;<i class='icon-unlock icon-white' title='Unlock' style='cursor: pointer;' onclick='unblock_ip("+'"'+params+'"'+");'></i>" 
                ]);
            }
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
        }
    });
}

function reset_vmtas(){
	processing_show();
	$.ajax({
		url: 'scripts.php?action=reset_vmtas',
		type: 'post',
		success: function(data) {
                var str=data.replace(/(\r\n|\n|\r)/gm,"");
				var ok=true;
                if(str==="0"){
					refresh();
                    $("#message-success").slideDown("slow");
                    setTimeout(function(){close_message_success();},3000);
					ok=false;
                }

				if(ok){
					$("#message-custom").text(str);
					$("#message-warning-custom").slideDown("slow");
					setTimeout(function(){close_message_warning_custom();},3000);
				}


                processing_hide();
            },
		error: function() {
			$("#message-error").slideDown("slow");
			setTimeout(function(){close_message_error();},3000);
			processing_hide();
		}
		
	});	
}

function show_servers(){
    var html= "<option value='0' disabled=''>Waiting for extracting Servers...</option>";
    $("#server").html(html);
    $.ajax({
        url: 'scripts.php?action=get_servers',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#server").html(html);
        }
    });
}

/*function show_ips(){
    var html= "<option value='0' disabled=''>Waiting for extracting IP Addresses...</option>";
    $("#address_ip").html(html);
    $.ajax({
        url: 'scripts.php?action=get_ips',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].address_ip+"'>"+list[i].address_ip+"</option>";
            }
            $("#address_ip").html(html);          
        }
    });
}*/

function show_ips(){
    var html= "<option value='0' disabled=''>Waiting for extracting IP Addresses...</option>";
    $("#address_ip").html(html);
    $.ajax({
        url: 'scripts.php?action=get_ips',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "";
            for (var i = 0; i < list.length; i++) {
                 html += "<option value='"+list[i].address_ip+"'>"+list[i].address_ip+"</option>";
            }
            $("#address_ip").html(html);
			ip_select_count();
        }
    });
}

function ip_select_count(){
    var nbips=$('#address_ip option:selected').length;
    var str='';
    if(nbips==0){
        str='No IP Address selected';
    }
    if(nbips==1){
        str=nbips+' IP Address selected';
    }
    if(nbips>1){
        str=nbips+' IP Addresses selected';
    }
    $("#ip_count").text(str);
}

function select_server_2(id){
    var html= "<option value='0' disabled=''>Waiting for extracting Servers...</option>";
    $("#server").html(html);
    $.ajax({
        url: 'scripts.php?action=get_servers',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#server").html(html);
            $('#server option[value=' + id + ']').attr('selected', 'selected');
            refresh();
        }
    });
}

/*function update_status(){
    $(".process-api").html("<span class='badge badge-info'>Processing</span>");
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        data: {
            id_server: document.getElementById("server").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            for (var i = 0; i < list.length; i++) {
                var params=list[i].id_vmta+","+list[i].provider+","+list[i].block_ip+","+list[i].address_ip;
                get_status(params);
            }
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
        }
    });
}*/

/*function get_status(params){
    
    params=params.split(",");
    var id_vmta=params[0];
    var provider=params[1];
    var block_ip=params[2];
    var address_ip=params[3];
    
    $("#idv-status"+id_vmta+"").html("<span class='badge badge-info'>Processing</span>");
    
    if(provider==="OVH.COM"){
        processing_show();
        $.ajax({
            url: 'scripts.php?action=GetIpStatusFromOVHAPI',
            type: 'GET',
            data: {
                block_ip: block_ip,
                address_ip: address_ip
            },
            success: function(data) {
                var str=data.replace(/(\r\n|\n|\r)/gm,"");
                var color="";
                if(str==="blockedForSpam"){
                    color="important";
                }
                if(str==="unblocked"){
                    color="success";
                }
                if(str==="unblocking"){
                    color="warning";
                }
                $("#idv-status"+id_vmta+"").html("<span class='badge badge-"+color+"'>"+str+"</span>");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                setTimeout(function(){close_message_error();},3000);
                processing_hide();
            }
        });
    }else{
        $("#idv-status"+id_vmta+"").html("<span class='badge'>Undefined</span>");
    }
}*/

/*function unblock_ip(params){
    
    var params_back=params;
    params=params.split(",");
    var id_vmta=params[0];
    var provider=params[1];
    var block_ip=params[2];
    var address_ip=params[3];
    
    $("#idv-status"+id_vmta+"").html("<span class='badge badge-info'>Processing</span>");
    
    
    if(provider==="OVH.COM"){
        processing_show();
        $.ajax({
            url: 'scripts.php?action=UnblockIpStatusFromOVHAPI',
            type: 'GET',
            data: {
                block_ip: block_ip,
                address_ip: address_ip
            },
            success: function(data) {
                var str=data.replace(/(\r\n|\n|\r)/gm,"");
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                setTimeout(function(){close_message_error();},3000);
                processing_hide();
            }
        });
    }
    
    get_status(params_back);
}*/

/*function unblock_all_ips(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        data: {
            id_server: document.getElementById("server").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var params;
            for (var i = 0; i < list.length; i++) {
                params=list[i].id_vmta+","+list[i].provider+","+list[i].block_ip+","+list[i].address_ip;
                unblock_ip(params);

            }
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
        }
    });
}*/