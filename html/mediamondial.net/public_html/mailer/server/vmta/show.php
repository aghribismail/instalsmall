﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VMTAs</title>
    <link type="text/css" href="..//static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="..//static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="..//static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="..//static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="..//static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="..//static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="..//static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="..//static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="..//static/scripts/datatables/jquery.dataTables.js"></script>
    <script src="scripts.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {       

                select_server_2(<?php echo $_GET['id_server'];?>);

                $('.datatable-1').dataTable();
                $('.datatable-1').find('thead th').css('width', 'auto');
                $('.dataTables_paginate').addClass("btn-group datatable-pagination");
                $('.dataTables_paginate > a').wrapInner('<span />');
                $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
                $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        } );
    </script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php include("../../inc.top_menu.php");  ?>
			</div>
        </div><!-- /navbar-inner -->
	</div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <?php include("../../left_menu.html");  ?>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Show VMTAs &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-plus-sign icon-white" title="Add" style="cursor: pointer;" onclick="location.href='add.php'"></i>&nbsp;&nbsp;<i class="icon-refresh icon-white" title="Refresh" style="cursor: pointer;" onclick="refresh();"></i>&nbsp;&nbsp;<i class="icon-random icon-white" title="Refresh Status" style="cursor: pointer;" onclick="update_status();"></i>&nbsp;&nbsp;<i class="icon-unlock icon-white" title="Unblock All" style="cursor: pointer;" onclick="unblock_all_ips();"></i>&nbsp;&nbsp;<button href='#confirm-reset-vmtas' role='button' data-toggle='modal' class="btn btn-danger btn-xs" type="button" name="submit" onclick="">Reset VMTAs for server <?php echo $name_server; ?></button><div class="processing" id="processing"></div></h3></div>
							<div class="module-body">
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Server</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="server" class="span5" onchange="refresh();">
                                                <option value="0">Select here...</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_servers();"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="module-body table">
                                <div class="alert" id="message-warning">
                                    <button type="button" class="close" onclick="close_message_warning();">×</button>
                                    <strong>Warning!</strong> Please verify your data
                                </div>
								<div class="alert" id="message-warning-custom">
                                    <button type="button" class="close" onclick="close_message_warning_custom();">×</button>
                                    <strong>Warning!</strong> <span id="message-custom"></span>
                                </div>
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <div class="alert alert-success" id="message-success">
                                    <button type="button" class="close" onclick="close_message_success();">×</button>
                                    <strong>Done!</strong> VMTA has been updated successfully
                                </div>
                                <table id="show" cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Address IP</th>
                                            <th>Host Name</th>
                                            <th>Provider</th>
                                            <th>Server</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Address IP</th>
                                            <th>Host Name</th>
                                            <th>Provider</th>
                                            <th>Server</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div><!--/.module-->
                    </div><!--/.content-->
                </div><!--/.span9-->

            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->
	<!-- Modal -->
	<div id="confirm-reset-vmtas" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		  <h3 id="myModalLabel">Confirmation</h3>
		</div>
		<div class="modal-body">
		  <p>You're about to Reset all Virtual MTAs in this Server</p>
		</div>
		<div class="modal-footer">
		  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="reset_vmtas()">Confirm</button>
		</div>
	 </div>

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>