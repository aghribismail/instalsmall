﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>VMTAs</title>
    <link type="text/css" href="..//static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="..//static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="..//static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="..//static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="..//static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="..//static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="..//static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="..//static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
			show_ips();
        });
    </script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php include("../../inc.top_menu.php");  ?>
			</div>
        </div><!-- /navbar-inner -->
	</div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <?php include("../../left_menu.html");  ?>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Create VMTA<div class="processing" id="processing"></div></h3></div>
                            <div class="module-body">
                                <div class="alert" id="message-warning">
                                    <button type="button" class="close" onclick="close_message_warning();">×</button>
                                    <strong>Warning!</strong> Please verify your data
                                </div>
                                <div class="alert" id="message-warning-custom">
                                    <button type="button" class="close" onclick="close_message_warning_custom();">×</button>
                                    <strong>Warning!</strong> <span id="message-custom"></span>
                                </div>
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <div class="alert alert-success" id="message-success">
                                    <button type="button" class="close" onclick="close_message_success();">×</button>
                                    <strong>Done!</strong> VMTA(s) has been created successfully
                                </div>
                                <form class="form-horizontal row-fluid" name="redirect_form" method="POST" action=""> 
									<div class="control-group">
                                        <label class="control-label" for="basicinput">VMTA Name</label>
                                        <div class="controls">
                                            <input type="text" id="name" name="name" placeholder="No spaces or special characters" class="span5">
                                            <span class="help-inline"><button class="btn btn-success" type="button" name="" onclick="generate_name();"><i class="icon-retweet icon-white"></i>&nbsp;Generate</button></span>
                                        </div>
                                    </div>
									<div class="control-group">
                                        <label class="control-label" for="basicinput">IP Addresses</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="address_ip" name="address_ip[]" size="10" multiple class="span5" style="float: left;" onchange="ip_select_count();"></select>
                                            <span class="" style="float: left;">&nbsp;&nbsp;&nbsp;<i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_ips();"></i></span><br/>
											&nbsp;&nbsp;&nbsp;<font size="1" id="ip_count">No Vmta Selected</font>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Host Name</label>
                                        <div class="controls">
                                            <input type="text" id="host_name" name="host_name" placeholder="" class="span5">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <button class="btn btn-primary" type="button" name="submit" onclick="add();"><i class="icon-pencil icon-white"></i>&nbsp;Create VMTA</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!--/.content-->
                </div><!--/.span9-->

            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->
	
    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>