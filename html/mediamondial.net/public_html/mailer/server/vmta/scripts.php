<?php
   include_once '../../account/session.php';
   include_once '../../../scripts/bd.php';
   
   if(isset($_GET['action'])){
    $action=$_GET['action'];
    if($action=='show')show();
	if($action=='add')add();
	if($action=='get_ips')get_ips();
    if($action=='get_servers')get_servers();
	if($action=='reset_vmtas')reset_vmtas();
    if($action=='GetIpStatusFromOVHAPI')ovh_api_ip_status();
    if($action=='UnblockIpStatusFromOVHAPI')ovh_api_ip_unblock();
   }

   function verify_name($id,$name,$id_server){
       $query = bd::query("SELECT * FROM server_vmta where name like '$name' && id_server='{$_SESSION['id-server']}' && id!='$id'");
       if(mysql_num_rows($query)>0){
           return true;
       }
       return false;
   }

   function add(){
        $name=trim($_POST["name"]);
        $address_ips=$_POST["address_ips"];
        $host_name=$_POST["host_name"];
        $active=1;
        $date_add=date('Y-m-d h:i:s');
        $date_set=date('Y-m-d h:i:s');
        
        $ok='0';
		$name_inital=$name;
		$address_ips=explode(';', $address_ips);
		foreach($address_ips as $address_ip){
			$cpt=1;	
			while(verify_name('0',$name,$id_server)){
				$cpt++;
				$name=$name_inital.$cpt;
			}

			$vmta_config_file = fopen($_SERVER["DOCUMENT_ROOT"]."/server/vmta/config.d/vmtas1.txt", "a+");
			if($vmta_config_file){
				$vMtaString =   "<virtual-mta $name>\r\n";
				$vMtaString.=   "\tsmtp-source-ip $address_ip\r\n";
				$vMtaString.=   "\thost-name $host_name\r\n"; 
				$vMtaString.=   "</virtual-mta>\r\n\n";
				$rep=bd::query("INSERT INTO server_vmta VALUES (NULL, '{$_SESSION['id-server']}', '$name', '$address_ip', '$address_ip', '$host_name', '1', '$active', '$date_add', '$date_set');");
				if($rep){
					fwrite($vmta_config_file, $vMtaString);
				}else{
					$ok='Error data base';
				}
			}else{
			   $ok="Unable to open VMTA config file !";
			}
		}

		if($ok=='0'){
			exec("sudo /etc/init.d/pmta reload",$output,$report);
			if($report!='0'){
				$ok="Cannot reload PowerMTA !";
			}
		}
        echo $ok;
   }
   
   function show(){
      $list=array();
      if($_GET["id_server"]==0){
          $data= bd::query("SELECT server_vmta.id as id_vmta,server_vmta.name,server_vmta.block_ip,server_vmta.address_ip,server_vmta.host_name,host_type,server_vmta.active,server.id,server.name as server_name,server.provider FROM server_vmta,server WHERE server_vmta.active=1 && server_vmta.id_server=server.id && server.active=1");
      }else{
          $data= bd::query("SELECT server_vmta.id as id_vmta,server_vmta.name,server_vmta.block_ip,server_vmta.address_ip,server_vmta.host_name,host_type,server_vmta.active,server.id,server.name as server_name,server.provider FROM server_vmta,server WHERE server_vmta.active=1 && server_vmta.id_server='{$_GET["id_server"]}' && server_vmta.id_server=server.id && server.active=1");
      }
      while ($row = mysql_fetch_object($data)) {
          $list[]=$row;
      }
      echo json_encode($list);
   }

   function reset_vmtas(){
		$ok='0';
		$vmta_config_file = fopen($_SERVER["DOCUMENT_ROOT"]."/server/vmta/config.d/vmtas1.txt", "a+");
		if($vmta_config_file){
			$rep=bd::query("DELETE FROM server_vmta where id_server={$_SESSION['id-server']} && host_type='1'");
			if($rep){
				ftruncate($vmta_config_file, 0);
				exec("sudo /etc/init.d/pmta reload",$output,$report);
			}else{
				$ok='Error data base';
			}
		}else{
		   $ok="Unable to open VMTA config file !";
		}
		echo $ok;
   }
   
   function get_servers(){
      $data=array();
      $query = bd::query("SELECT id,name FROM server WHERE active=1");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }

   function get_ips(){
      $data=array();
      $query = bd::query("SELECT address_ip FROM server_vmta WHERE id_server='{$_SESSION['id-server']}' && host_type='0' && active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }


   
   function ovh_api_ip_status(){
       $block_ip=$_GET['block_ip'];
       $address_ip=$_GET['address_ip'];
       $hdl= file_get_contents("http://72.55.138.36/app/manager/server/api-ovh/scripts.php?action=GetIpStatusFromOVHAPI&block_ip=$block_ip&address_ip=$address_ip");
       $hdl=explode('{', $hdl);
       $hdl=explode('}', $hdl[1]);
       $hdl='{'.$hdl[0].'}';
       $obj = json_decode($hdl);
       echo $obj->state;
   }
   
   function ovh_api_ip_unblock(){
       $block_ip=$_GET['block_ip'];
       $address_ip=$_GET['address_ip'];
       $hdl= file_get_contents("http://72.55.138.36/app/manager/server/api-ovh/scripts.php?action=UnblockIpStatusFromOVHAPI&block_ip=$block_ip&address_ip=$address_ip");
       $hdl=explode('{', $hdl);
       $hdl=explode('}', $hdl[1]);
       $hdl='{'.$hdl[0].'}';
       $obj = json_decode($hdl);
       echo $obj->state;
   }
   
