$(document).ready(function(){
    razmessage();
    show_sponsors();
});

function razmessage(){
  $("#message-warning").hide();
  $("#message-warning-custom").hide();
  $("#message-error").hide();
  $("#message-success").hide();
}

function close_message_warning(){
    $("#message-warning").slideUp("slow");
}

function close_message_warning_custom(){
    $("#message-warning-custom").slideUp("slow");
}

function close_message_error(){
    $("#message-error").slideUp("slow");
}

function close_message_success(){
    $("#message-success").slideUp("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function check_data(){
   if(document.getElementById("sponsor").value==="0")return false;
   if(document.getElementById("offer").value==="0")return false;
   return true;
}

function clear_data(){
   document.getElementById("sponsor").value="0";
   document.getElementById("offer").value="0";
}

function add(){
    if(check_data()){
        processing_show();
        document.forms["upload_form"].submit();
    }else{
        $("#message-warning").slideDown("slow");
    }
}

function refresh_initial(id){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        data: {
            id_server: id,
            id_sponsor: document.getElementById("sponsor").value,
            id_offer: document.getElementById("offer").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var html='';
            if(list.length==0){
                html="<li class='span2'>No image available</li>";
                $("#show").html(html);
            }else{
                for (var i = 0; i < list.length; i++) {
                    html += "<li class='span2'><a href='#myModal' role='button' class='thumbnail' data-toggle='modal' onclick='show_image("+list[i].id+")'><img src='"+list[i].link+"' alt=''/></a></li>";
                }
                $("#show").html(html);
            }
            
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        data: {
            id_server: document.getElementById("server").value,
            id_sponsor: document.getElementById("sponsor").value,
            id_offer: document.getElementById("offer").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var html='';
            if(list.length==0){
                html="<li class='span2'>No image available</li>";
                $("#show").html(html);
            }else{
                for (var i = 0; i < list.length; i++) {
                    html += "<li class='span2'><a href='#myModal' role='button' class='thumbnail' data-toggle='modal' onclick='show_image("+list[i].id+")'><img src='"+list[i].link+"' alt=''/></a></li>";
                }
                $("#show").html(html);
            }
            
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}


function select_server(id){
    var html= "<option value='0' disabled=''>Waiting for extracting Servers...</option>";
    $("#server").html(html);
    $.ajax({
        url: 'scripts.php?action=get_servers',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#server").html(html);
            $('#server option[value=' + id + ']').attr('selected', 'selected');
        }
    });
}

function show_sponsors(){
    var html= "<option value='0' disabled=''>Waiting for extracting Sponsors...</option>";
    $("#sponsor").html(html);
    $.ajax({
        url: 'scripts.php?action=get_sponsors',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#sponsor").html(html);          
        }
    });
}

function show_offers(){
    var html= "<option value='0' disabled=''>Waiting for extracting Offers...</option>";
    $("#offer").html(html);
    $.ajax({
        url: 'scripts.php?action=get_offers',
        type: 'get',
        data: {
            sponsor: document.getElementById("sponsor").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#offer").html(html);          
        }
    });
}

function show_image(id){
    var html= "<center><p><div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>Waiting for extracting Image...</div></p></center>";
    $("#preview").html(html);
    $.ajax({
        url: 'scripts.php?action=get_image',
        type: 'get',
        data: {
            image: id
        },
        success: function(data) {
            var list = JSON.parse(data);
            var link=list[0].link;
            var str=link.split("/");
            var link_final="http://[Domain]/app/image/"+str[5];
            var html = "<p><strong>Server:</strong> "+list[0].server_name+"</p><p><strong>Sponsor:</strong> "+list[0].sponsor_name+"</p><p><strong>Offer:</strong> "+list[0].offer_name+"</p><p><strong>Image Link:</strong> "+link_final+"</p><center><p><img src='"+list[0].link+"' alt=''/></p></center>";
            var html2="<a href='"+list[0].link+"' role='button' class='btn btn-primary' data-toggle='' download><i class='icon-download icon-white'></i>&nbsp;Download Image</a><button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button>";
            $("#preview").html(html);          
            $("#download").html(html2);          
        },
        error: function() {
            $("#preview").html("<center><p>There was an error while executing your request</p></center>");
            $("#download").html("<button class='btn' data-dismiss='modal' aria-hidden='true'>Close</button>");
        }
    });
}

function download_file(){
    $.fileDownload('http://184.107.183.226/app/image/6846.png', {
    successCallback: function (url) {
        alert('You just got a file download dialog or ribbon for this URL :' + url);
    },
    failCallback: function (html, url) {
        alert('Your file download just failed for this URL:' + url + '\r\n' +
                'Here was the resulting error HTML: \r\n' + html
                );
    }
    });
}