﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Offers</title>
    <link type="text/css" href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="/static/scripts/jquery-1.9.1.min.js"></script>
	<script src="/static/scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
	<script src="/static/scripts/datatables/jquery.dataTables.js"></script>
        <script src="scripts.js" type="text/javascript"></script>
	<script>
            $(document).ready(function() {
                
                    select_sponsor_2(<?php echo $_GET['id_sponsor'];?>);
                    
                    $('.datatable-1').dataTable();
                    $('.datatable-1').find('thead th').css('width', 'auto');
                    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
                    $('.dataTables_paginate > a').wrapInner('<span />');
                    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
                    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');

            } );
	</script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php include("../inc.top_menu.php");  ?>
			</div>
        </div><!-- /navbar-inner -->
	</div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <?php include("../left_menu.html");  ?>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Show Offers &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-refresh icon-white" title="Refresh" style="cursor: pointer;" onclick="refresh();"></i><div class="processing" id="processing"></div></h3></div>
                            <div class="module-body">
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Sponsor</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="sponsor" class="span5" onchange="refresh();">
                                                <option value="0">Select here..</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_sponsors();"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="module-body table">
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <table id="show" cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                                    <thead>
                                        <tr>
                                            <th>SID</th>
                                            <th>Name</th>
                                            <th>Sponsor</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>SID</th>
                                            <th>Name</th>
                                            <th>Sponsor</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div><!--/.module-->
                    </div><!--/.content-->
                </div><!--/.span9-->

            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>