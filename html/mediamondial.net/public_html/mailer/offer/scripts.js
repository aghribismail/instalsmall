$(document).ready(function(){
    razmessage();
});

function razmessage(){
  $("#message-warning").hide();
  $("#message-warning-custom").hide();
  $("#message-error").hide();
  $("#message-success").hide();
}

function close_message_warning(){
    $("#message-warning").slideUp("slow");
}

function close_message_warning_custom(){
    $("#message-warning-custom").slideUp("slow");
}

function close_message_error(){
    $("#message-error").slideUp("slow");
}

function close_message_success(){
    $("#message-success").slideUp("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function check_data(){
   if(document.getElementById("sponsor").value==="0")return false;
   if(document.getElementById("offer").value==="0")return false;
   if(document.getElementById("server").value==="0")return false;
   if(document.getElementById("domain").value==="0")return false;
   if(document.getElementById("traffic_source").value==="")return false;
   if(document.getElementById("description").value==="")return false;
   return true;
}

function clear_data(){
   document.getElementById("sponsor").value="0";
   document.getElementById("offer").value="0";
   document.getElementById("server").value="0";
   document.getElementById("domain").value="0";
   document.getElementById("traffic_source").value="";
   document.getElementById("description").value="";
}

function copy_link1(){
    $("#offer_page").focus().select();
}

function copy_link2(){
    $("#offer_unsubscribe").focus().select();
}

function copy_link3(){
    $("#server_unsubscribe").focus().select();
}


function create_redirect(){
    if(check_data()){
        processing_show();
        $.ajax({
            url: 'scripts.php?action=create_redirect',
            type: 'post',
            data: {
                id_sponsor: document.getElementById("sponsor").value,
                id_offer: document.getElementById("offer").value,
                id_server: document.getElementById("server").value,
                domain : document.getElementById("domain").value,
                traffic_source : document.getElementById("traffic_source").value,
                description : document.getElementById("description").value
            },
            success: function(data) {
                var str=data.replace(/(\r\n|\n|\r)/gm,"");
                if(str==="1"){ 
                    $("#message-error").slideDown("slow");
                    setTimeout(function(){close_message_error();},3000);
                }else{
                    str=str.split('AHMEDTOTO');
                    $("#offer_page").val(str[0]);
                    $("#offer_unsubscribe").val(str[1]);
                    $("#server_unsubscribe").val(str[2]);
                    $("#message-success").slideDown("slow");
                    setTimeout(function(){close_message_success();},3000);
                    clear_data();
                }
                processing_hide();
            },
            error: function() {
                $("#message-error").slideDown("slow");
                setTimeout(function(){close_message_error();},3000);
                processing_hide();
            }
            
        }); 
    }else{
        $("#message-warning").slideDown("slow");
        setTimeout(function(){close_message_warning();},3000);
    }
}

function refresh(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        data: {
            id_sponsor: document.getElementById("sponsor").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            $('#show').dataTable().fnClearTable();
            for (var i = 0; i < list.length; i++) {
                $('#show').dataTable().fnAddData([ 
                    list[i].sid,
                    list[i].name,
                    list[i].sponsor_name,
                    "<span class='badge badge-success'>Active</span>",
                    "<i class='icon-refresh icon-white' title='Refresh' style='cursor: pointer;' onclick=''></i>" 
                ]);
            }
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
        }
    });
}

/*function refresh(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'post',
        data: {
            //id_news: document.getElementById("news").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var html= "<tr class='heading'><td class='cell-icon'></td><td class='cell-title'>Sponsor Name</td><td class='cell-status hidden-phone hidden-tablet'>Status</td><td class='cell-time align-right'>Options</td></tr>";
            var inp1='';
            var inp2='';
            for (var i = 0; i < list.length; i++) {
                switch (list[i].platform){
                    case 'HitPath': inp1='username';inp2='password';break;
                    case 'Cake': inp1='u';inp2='p';break;
                    default : inp1='xxx';inp2='xxx';break;
                }
                html+="<tr class='task'><td class='cell-icon'><i class='icon-checker high'></i></td><td class='cell-title'><div>"+list[i].name+"</div></td><td class='cell-status hidden-phone hidden-tablet'><b class=''>Active</b></td><td class='cell-time align-right'><form id='' action='"+list[i].login_page+"' method='POST' target='_blank'><input type='hidden' id='username' name='"+inp1+"' value='"+list[i].login+"' /><input type='hidden' id='password' name='"+inp2+"' value='"+list[i].password+"' /><input class='btn btn-primary' type='submit' value='Go'/></form></td></tr>";
            }
            $("#show").html(html);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}*/

function show_sponsors(){
    html= "<option value='0' disabled=''>Waiting for extracting Sponsors...</option>";
    $("#sponsor").html(html);
    $.ajax({
        url: 'scripts.php?action=get_sponsors',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#sponsor").html(html);
        }
    });
}

function select_sponsor(id){
    html= "<option value='0' disabled=''>Waiting for extracting Sponsors...</option>";
    $("#sponsor").html(html);
    $.ajax({
        url: 'scripts.php?action=get_sponsors',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here..</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#sponsor").html(html);
            $('#sponsor option[value=' + id + ']').attr('selected', 'selected');
        }
    });
}

function select_sponsor_2(id){
    html= "<option value='0' disabled=''>Waiting for extracting Sponsors...</option>";
    $("#sponsor").html(html);
    $.ajax({
        url: 'scripts.php?action=get_sponsors',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here..</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#sponsor").html(html);
            $('#sponsor option[value=' + id + ']').attr('selected', 'selected');
            refresh();
        }
    });
}


function show_offers(){
    var html= "<option value='0' disabled=''>Waiting for extracting Offers...</option>";
    $("#offer").html(html);
    $.ajax({
        url: 'scripts.php?action=get_offers',
        type: 'get',
        data: {
            sponsor: document.getElementById("sponsor").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#offer").html(html);          
        }
    });
}

function show_servers(){
    var html= "<option value='0' disabled=''>Waiting for extracting Servers...</option>";
    $("#server").html(html);
    $.ajax({
        url: 'scripts.php?action=get_servers',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#server").html(html);
        }
    });
}

function show_domains(){
    var html= "<option value='0' disabled=''>Waiting for extracting Domains...</option>";
    $("#domain").html(html);
    $.ajax({
        url: 'scripts.php?action=get_domains',
        type: 'get',
        data: {
            server: document.getElementById("server").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].host_name+"'>"+list[i].host_name+"</option>";
            }
            $("#domain").html(html);          
        }
    });
}