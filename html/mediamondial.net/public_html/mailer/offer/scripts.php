<?php
   include_once '../account/session.php';
   include_once '../../scripts/bd.php';
   
   if(isset($_GET['action'])){
    $action=$_GET['action'];
    if($action=='show')show();
    if($action=='create_redirect')create_redirect();
    if($action=='get_sponsors')get_sponsors();
    if($action=='get_offers')get_offers();
    if($action=='get_servers')get_servers();
    if($action=='get_domains')get_domains();
   }
   
   function create_redirect(){
        $id_sponsor=$_POST["id_sponsor"];
        $id_offer=$_POST["id_offer"];
        $id_server=$_POST["id_server"];
        $domain=$_POST["domain"];
        $traffic_source=$_POST["traffic_source"];
        $description=$_POST["description"];
        $date_add=date('Y-m-d H:i:s');
        
        $link1='';
        $link2='';
        $link3='';
        
        $query = bd::query("SELECT platform FROM sponsor WHERE id='$id_sponsor'");
        $row=mysql_fetch_array($query);
        $platform=$row['platform'];
        
        $query = bd::query("SELECT page_link,unsub_link FROM offer WHERE id='$id_offer'");
        $row=mysql_fetch_array($query);
        
        if($row['page_link']!=null){
            $link=$row['page_link'];
            $http_link='';
            $track_link='';
            if($platform=='HitPath'){
                $str=explode('&c1=', $link);
                $http_link=$str[0];
                $track_link="&c1=".$_SESSION['id-mailer']."&c2=".$traffic_source."&c3=".$description;
            }
            if($platform=='Cake'){
                $str=explode('&s1=', $link);
                $http_link=$str[0];
                $track_link="&s1=".$_SESSION['id-mailer']."&s2=".$traffic_source."&s3=".$description;
            }
            
            $link_global=$http_link.$track_link;
            $track_id=RandomPrefix(25);
            $date_add=date('Y-m-d h:i:s');
            $res = bd::query("INSERT INTO redirect VALUES(NULL,'{$_SESSION["id-server"]}','{$_SESSION["id-mailer"]}','{$_POST["offer"]}','-1','$link_global','[LandingPage]','$track_id','$date_add')");
            if($res){
                $link1='http://'.$domain.'/app/redirection/rdt.php?track='.$track_id.'&id_campaign=-1&id_list=-1&id_user=-1';
            }else{
                $link1='NULL';
            }
        }
        
        if($row['unsub_link']!=null){
            $link=$row['unsub_link'];
            $track_id=RandomPrefix(25);
            $res = bd::query("INSERT INTO redirect VALUES(NULL,'{$_SESSION["id-server"]}','{$_SESSION["id-mailer"]}','{$_POST["offer"]}','-1','$link','[Unsubscribe]','$track_id','$date_add')");
            if($res){
                $link2='http://'.$domain.'/app/redirection/rdt.php?track='.$track_id.'&id_campaign=-1&id_list=-1&id_user=-1';
            }else{
                $link2='NULL';
            }
        }
        
        if($link1!='NULL' && $link2!='NULL'){
            $link="http://".$domain."/";
            $track_id=RandomPrefix(25);
            $res = bd::query("INSERT INTO redirect VALUES(NULL,'{$_SESSION["id-server"]}','{$_SESSION["id-mailer"]}','{$_POST["offer"]}','-1','$link','[UnsubServer]','$track_id','$date_add')");
            if($res){
                $link3='http://'.$domain.'/app/redirection/rdt.php?track='.$track_id.'&id_campaign=-1&id_list=-1&id_user=-1';
            }else{
                $link3='NULL';
            }
        }
        
        if($link1!='NULL' && $link2!='NULL' && $link3!='NULL'){
            echo $link1.'AHMEDTOTO'.$link2.'AHMEDTOTO'.$link3;
        }else{
            echo '1';
        }
   }
   
   function show(){
        $list=array();
        if($_GET["id_sponsor"]==0){
            $data= bd::query("SELECT offer.id as id_offer,offer.sid,offer.name,offer.active,sponsor.id,sponsor.name as sponsor_name FROM offer,sponsor WHERE offer.active=1 && offer.id_sponsor=sponsor.id && offer.active=1 && sponsor.active=1");
        }else{
            $data= bd::query("SELECT offer.id as id_offer,offer.sid,offer.name,offer.active,sponsor.id,sponsor.name as sponsor_name FROM offer,sponsor WHERE offer.active=1 && offer.id_sponsor=sponsor.id && offer.id_sponsor='{$_GET["id_sponsor"]}' && offer.active=1 && sponsor.active=1");
        }
        while ($row = mysql_fetch_object($data)) {
            $list[]=$row;
        }
        echo json_encode($list);
    }
   
   /*function show(){
      $data=array();
      $query = bd::query("SELECT name,login_page,login,password,platform FROM sponsor WHERE active=1");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }*/
   
   function get_sponsors(){
      $data=array();
      $query = bd::query("SELECT id,name FROM sponsor WHERE active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function get_offers(){
      $data=array();
      $query = bd::query("SELECT id,name FROM offer WHERE id_sponsor='{$_GET["sponsor"]}' &&  active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function get_servers(){
      $data=array();
      $query = bd::query("SELECT id,name FROM server WHERE active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function get_domains(){
      $data=array();
      $query = bd::query("SELECT host_name FROM server_vmta WHERE id_server='{$_GET["server"]}' && active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function RandomPrefix($length){
        $random= "";
        srand((double)microtime()*1000000);
        //$data = "bc";
        $data = "0123456789abcdefghijklmnopqrstuvwxyz";
        //$data .= "fgh";
        for($i = 0; $i < $length; $i++){
            $random .= substr($data, (rand()%(strlen($data))), 1);
        }
        return $random;
    }
   
