$(document).ready(function(){
    razmessage();
});

function razmessage(){
  $("#message-warning").hide();
  $("#message-warning-custom").hide();
  $("#message-error").hide();
  $("#message-success").hide();
}

function close_message_warning(){
    $("#message-warning").slideUp("slow");
}

function close_message_warning_custom(){
    $("#message-warning-custom").slideUp("slow");
}

function close_message_error(){
    $("#message-error").slideUp("slow");
}

function close_message_success(){
    $("#message-success").slideUp("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function refresh_stats_campaign(){
    
}

function refresh_stats_campaign(id_campaign){
    refresh_campaign_selected(id_campaign);
    refresh_campaign_processed(id_campaign);
    refresh_campaign_delivered(id_campaign);
    
    refresh_campaign_bounces(id_campaign);
    refresh_campaign_hardbounce(id_campaign);
    refresh_campaign_softbounce(id_campaign);
    
    refresh_campaign_opens(id_campaign);
    refresh_campaign_opens_rate(id_campaign);
    refresh_campaign_clicks(id_campaign);
    refresh_campaign_clicks_rate(id_campaign);
    refresh_campaign_bounces(id_campaign);
    refresh_campaign_bounces_rate(id_campaign);
    refresh_campaign_clicks_offerpage(id_campaign);
    refresh_campaign_clicks_offerpage_rate(id_campaign);
    refresh_campaign_clicks_offerunsub(id_campaign);
    refresh_campaign_clicks_offerunsub_rate(id_campaign);
    refresh_campaign_clicks_serverunsub(id_campaign);
    refresh_campaign_clicks_serverunsub_rate(id_campaign);
}

function refresh_campaign_selected(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_selected',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-selected").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_processed(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_processed',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-processed").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_delivered(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_delivered',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-delivered").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_bounces(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_bounces',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-bounces").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_hardbounce(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_hardbounce',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-hardbounce").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_softbounce(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_softbounce',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-softbounce").text(lisibilite_nombre(str));
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_opens(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_opens',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-opens").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_opens_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_opens_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            var cent=str+"%";
            $("#opens-rate").text(cent);
            $("#opens-rate-bar").css("width",cent);
            var cent2=100-str;
            cent2+="%";
            $("#not-opens-rate").text(cent2);
            $("#not-opens-rate-bar").css("width",cent2);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#clicks-rate").text(str);
            $("#clicks-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_bounces_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_bounces_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#bounces-rate").text(str);
            $("#bounces-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerpage(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerpage',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks-offerpage").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerpage_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerpage_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#clicks-offerpage-rate").text(str);
            $("#clicks-offerpage-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerunsub(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerunsub',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks-offerunsub").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_offerunsub_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_offerunsub_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#clicks-offerunsub-rate").text(str);
            $("#clicks-offerunsub-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_serverunsub(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_serverunsub',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#total-clicks-serverunsub").text(str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function refresh_campaign_clicks_serverunsub_rate(id_campaign){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=get_campaign_clicks_serverunsub_rate',
        type: 'get',
        data: {
            id_campaign: id_campaign
        },
        success: function(data) {
            var str=data.replace(/(\r\n|\n|\r)/gm,"");
            $("#clicks-serverunsub-rate").text(str);
            $("#clicks-serverunsub-rate-bar").css("width",str);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            processing_hide();
        }
    });
}

function lisibilite_nombre(nbr){
    var nombre = ''+nbr;
    var retour = '';
    var count=0;
    for(var i=nombre.length-1 ; i>=0 ; i--){
            if(count!=0 && count % 3 == 0)
                    retour = nombre[i]+' '+retour ;
            else
                    retour = nombre[i]+retour ;
            count++;
    }
    return retour;
}

function lisibilite_nombre(nbr){
    var nombre = ''+nbr;
    var retour = '';
    var count=0;
    for(var i=nombre.length-1 ; i>=0 ; i--){
            if(count!=0 && count % 3 == 0)
                    retour = nombre[i]+' '+retour ;
            else
                    retour = nombre[i]+retour ;
            count++;
    }
    return retour;
}



