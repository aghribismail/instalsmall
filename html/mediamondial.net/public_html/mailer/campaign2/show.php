﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show Campaigns</title>
    <link type="text/css" href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="/static/scripts/jquery-1.9.1.min.js"></script>
	<script src="/static/scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="/static/bootstrap/js/bootstrap.min.js"></script>
	<script src="/static/scripts/datatables/jquery.dataTables.js"></script>
        <script src="scripts.js" type="text/javascript"></script>
        <script type="text/javascript">
         /*var c=0;
         var t;
         var timer_is_on=0;
         function timedCount(){
             //alert("grdrg");
             refresh();
             t=setTimeout("timedCount()",2000);
         }
         function doTimer(){
             if (!timer_is_on){
               timer_is_on=1;
               timedCount();
             }
         }*/
        </script>
	<script>
            $(document).ready(function() {
                refresh();
                $('.datatable-1').dataTable( {
                    "aaSorting": [[0, 'desc']]
                } );
                $('.datatable-1').dataTable();
                $('.dataTables_paginate').addClass("btn-group datatable-pagination");
                $('.dataTables_paginate > a').wrapInner('<span />');
                $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
                $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
            } );
	</script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse"><i class="icon-reorder shaded"></i></a>
                <!--a class="brand" href="/"><img src="/static/images/logo.jpg"/></a-->
                <a class="brand" href="/"><img class="logo" src="/static/images/logo.png"></a>
                
                <div class="nav-collapse collapse navbar-inverse-collapse">
                    <ul class="nav nav-icons">
                        <li><a href="#"><i><?php echo $name_server; ?></i></a></li>
                    </ul>
                    
                    <ul class="nav pull-right">
                        <li><a href="/campaign/send.php" >Send Campaign</a></li>
                        <li class="active"><a href="/campaign/show.php" >Show Campaigns</a></li>
                        <li><a href="/pmta/manage.php" >Manage PMTA</a></li>
                        <li><a href="/pmta/monitor.php" >Monitoring PMTA</a></li>
                        <li><a href="/image/upload.php" >Upload Images</a></li>
                        <li><a href="/image/show.php" >Show Images</a></li>
                        <li class="nav-user dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/static/images/user.png" class="nav-avatar" /><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                    <li><a href="#"><?php echo $username_mailer; ?></a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Account Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/account/logout.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <div class="wrapper">
        <div class="container" style="width: 100%">
            <div class="row" style="width: 100%">
                <div class="span12" style="width: 100%">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Show Campaigns&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-refresh icon-white" title="Refresh" style="cursor: pointer;" onclick="refresh();"></i><div class="processing" id="processing"></div></h3></div>
                            <div class="module-body table">
                                <div class="alert" id="message-warning">
                                    <button type="button" class="close" onclick="close_message_warning();">×</button>
                                    <strong>Warning!</strong> Please verify your data
                                </div>
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <div class="alert alert-success" id="message-success">
                                    <button type="button" class="close" onclick="close_message_success();">×</button>
                                    <strong>Done!</strong> Data List has been updated successfully
                                </div>
                                <table id="show" cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display campaign_table" width="100%">
                                    <thead >
                                        <tr>
                                            <th>ID</th>
                                            <th>Date Send</th>
                                            <th>Offer</th>
                                            <th>News</th>
                                            <th>ISP</th>
                                            <th>Data List</th>
                                            <th>From-Count</th>
                                            <th>Sent</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Date Send</th>
                                            <th>Offer</th>
                                            <th>News</th>
                                            <th>ISP</th>
                                            <th>Data List</th>
                                            <th>From-Count</th>
                                            <th>Sent</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div><!--/.module-->
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->
    
        <!-- Modal -->
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel">Confirmation</h3>
            </div>
            <div class="modal-body">
              <p>You're about to Stop The Campaign number: <strong id="stop-campaign-id">0</strong></p>
            </div>
            <div class="modal-footer">
              <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
              <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="stop_campaign($('#stop-campaign-id').html())">Confirm</button>
            </div>
         </div>

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
	
</body>