<?php
   include_once '../account/session.php';
   include_once '../../scripts/bd.php';
   session_start();
   $id_server=$_SESSION['id-server'];
   $id_mailer=$_SESSION['id-mailer'];
   $username_mailer=$_SESSION['username-mailer'];
   
   if(isset($_GET['action'])){
    $action=$_GET['action'];
    if($action=='get_servers')get_servers();
    if($action=='get_domains')get_domains();
    if($action=='get_vmtas')get_vmtas();
    if($action=='get_sponsors')get_sponsors();
    if($action=='get_offers')get_offers();
    if($action=='get_news')get_news();
    if($action=='get_isps')get_isps();
    if($action=='get_data_lists')get_data_lists();
    if($action=='show')show();
    if($action=='edit')edit();
    if($action=='stop_campaign')stop_campaign();
    if($action=='get_real_sent')get_real_sent();
    if($action=='get_real_status')get_real_status();
   }
   
   function get_servers(){
      $data=array();
      $query = bd::query("SELECT id,name FROM server WHERE active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function get_domains(){
      $data=array();
      $query = bd::query("SELECT host_name FROM server_vmta WHERE id_server='{$_GET["server"]}' && active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function get_vmtas(){
        $data=array();

        $servers  =   arraykeysToInSqlClause($_GET['server']);
        $query = bd::query("SELECT V.id,V.name,V.address_ip,V.host_name,S.id as id_server,S.name as name_server,S.main_ip FROM server_vmta V,server S WHERE S.id=V.id_server and  id_server in $servers && S.active=1 && V.active=1;");

        while ($row = mysql_fetch_object($query)) {
            $data[]=$row;
        }
        echo json_encode($data);
   }
   
   function get_sponsors(){
        $data=array();
        $query = bd::query("SELECT id,name FROM sponsor WHERE active='1'");
        while ($row = mysql_fetch_object($query)) {
            $data[]=$row;
        }
        echo json_encode($data);
   }
   
   function get_offers(){
        $data=array();
        $query = bd::query("SELECT id,name FROM offer WHERE id_sponsor='{$_GET["sponsor"]}' &&  active='1'");
        while ($row = mysql_fetch_object($query)) {
            $data[]=$row;
        }
        echo json_encode($data);
   }
   
   function get_news(){
      $data=array();
      $query = bd::query("SELECT id,name FROM data_news WHERE active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function get_isps(){
      $data=array();
      $query = bd::query("SELECT id,name FROM data_isp WHERE active='1'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function get_data_lists(){
      $data=array();
      if(!empty($_GET["news"]) && !empty($_GET["isp"])  ){
        calculate_data_isp();
        $query = bd::query("SELECT id,name,count_isp FROM data_list WHERE id_news='{$_GET["news"]}' && data_table!='' && active='1'");
        while ($row = mysql_fetch_object($query)) {
            $data[]=$row;
        } 
      }
      echo json_encode($data);
   }
   
   function calculate_data_isp(){
      $query = bd::query("SELECT regex FROM data_isp WHERE id={$_GET["isp"]}");
      $row=  mysql_fetch_array($query);
      $regexisp=$row['regex'];
      $query = bd::query("SELECT id,data_table FROM data_list WHERE id_news='{$_GET["news"]}' && data_table!='' && active='1' ");
      while($row=mysql_fetch_row($query)){
          bd::query("UPDATE data_list set count_isp=(select count(*) from $row[1] where email_address REGEXP '$regexisp') where id='$row[0]' ");
      } 
   }
    
   function show(){
      $data=array();
      $query = bd::query("SELECT campaign.id, campaign.date_send, campaign.id_offer, campaign.id_news, campaign.id_isp, campaign.id_data_list, campaign.data_from, campaign.data_count, campaign.data_sent, campaign.status, offer.name as offer_name, data_news.name as news_name, data_isp.name as isp_name, data_list.name as list_name FROM campaign, offer, data_news, data_isp, data_list WHERE campaign.id_mailer='{$_SESSION['id-mailer']}' && campaign.id_server='{$_SESSION['id-server']}' && campaign.id_offer=offer.id && campaign.id_news=data_news.id && campaign.id_isp=data_isp.id && campaign.id_data_list=data_list.id   ORDER BY campaign.date_send DESC");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function edit(){
      $data=array();
      $query = bd::query("SELECT * FROM campaign WHERE id='{$_GET["id_campaign"]}' && id_mailer='{$_SESSION['id-mailer']}'");
      while ($row = mysql_fetch_object($query)) {
          $data[]=$row;
      }
      echo json_encode($data);
   }
   
   function stop_campaign(){
       $res=bd::query("select status from campaign where id='{$_GET["id_campaign"]}'");
        $rowstatus= mysql_fetch_array($res);
        if($rowstatus['status']=='Sending'){
            $res = bd::query("UPDATE campaign set status='Stopped' where id='{$_GET["id_campaign"]}'");
            if($res){
                echo "Campaign {$_GET["id_campaign"]} Stopped";
            }else{
                echo "There was an error Stopping your Campaign";
            }
        }else{
            echo "You cannot stop this Campaign! Try again";
        }
      
   }
   
   function get_real_sent(){
       $res=bd::query("select data_sent from campaign where id='{$_GET["id_campaign"]}'");
       $rowsent= mysql_fetch_array($res);
       echo $rowsent['data_sent'];
   }
   
   function get_real_status(){
       $res=bd::query("select status from campaign where id='{$_GET["id_campaign"]}'");
       $rowstatus= mysql_fetch_array($res);
       echo $rowstatus['status'];
   }

   function RandomPrefix($length){
        $random= "";
        srand((double)microtime()*1000000);
        $data = "bc";
        $data .= "adeijklmnopqrstuvwxyz";
        $data .= "fgh";
        for($i = 0; $i < $length; $i++){
            $random .= substr($data, (rand()%(strlen($data))), 1);
        }
        return $random;
    }
    
    

function arraykeysToInSqlClause($p_arra_keys)
{
        $strKeys		=	'';
        foreach($p_arra_keys as $key):
                $strKeys	=	$strKeys.','.$key;
        endforeach;
        $strKeys		=	substr($strKeys,1);
        $strKeys		=	'('.$strKeys.')';

        return $strKeys;
}
