<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse"><i class="icon-reorder shaded"></i></a>
	<a class="brand" href="/"><img class="logo" src="/static/images/logo.png"></a>
	
	<div class="nav-collapse collapse navbar-inverse-collapse">
		<ul class="nav nav-icons">
			<li><a href="/"><i><?php echo $name_server; ?></i></a></li>
		</ul> 
		
		<ul class="nav pull-right">
			<li><a href="/campaign/send.php" target="_blank" >Send Campaign</a></li>
			<li><a href="/campaign/show.php" target="_blank">Show Campaigns</a></li>
			<li><a href="/pmta/manage.php" target="_blank">Manage PMTA</a></li>
			<li><a href="/pmta/monitor.php" target="_blank">Monitoring PMTA</a></li>
			<li><a href="/server/show.php" target="_blank">Show Servers</a></li>
			<li><a href="/image/show.php" target="_blank">Show Images</a></li>
			<li><a href="/image/upload.php" target='_blank'>Upload Images</a></li>
			<li class="nav-user dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="/static/images/user.png" class="nav-avatar" /><b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="#"><?php echo $username_mailer; ?></a></li>
					<li><a href="/account/logout.php">Logout</a></li>
				</ul>
			</li>
		</ul>
	</div><!-- /.nav-collapse -->
