﻿<!DOCTYPE html>
<?php 
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Send Campaign</title>
    <link type="text/css" href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="/static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            select_server(<?php echo $id_server;?>);
            select_domains(<?php echo $id_server;?>);
            show_vmtas();
            show_sponsors();
            show_news();
            show_isps();
            body2();
        } );
    </script>
</head>
<body>
	<div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php include("../inc.top_menu.php");  ?>
			</div>
        </div><!-- /navbar-inner -->
	</div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
            <form class="form-horizontal row-fluid" id="sendform" name="sendform" method="POST" action="" target="frame">
                <div class="span6">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Header & Creative Setting <div class="processing" id="processing"></div></h3></div>
                            <div class="module-body">
                                <div class="alert" id="message-warning">
                                    <button type="button" class="close" onclick="close_message_warning();">×</button>
                                    <strong>Warning!</strong> Please verify your data
                                </div>
                                <div class="alert" id="message-warning-custom">
                                    <button type="button" class="close" onclick="close_message_warning_custom();">×</button>
                                    <strong>Warning!</strong> <span id="message-custom"></span>
                                </div>
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div><br/>
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">From Name</label>
                                        <div class="controls">
                                            <input type="text" id="from_name" name="from_name" value="admin" placeholder="" class="span10">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Subject</label>
                                        <div class="controls">
                                            <input type="text" id="subject" name="subject" value="[Vmta] [Ip] [Domain]" placeholder="" class="span10">
                                            <span class="help-inline"><i href='#subject-info' class='icon-info-sign icon-white' title='Infos' role='button' data-toggle='modal' style='cursor: pointer;' onclick=''></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">From Email</label>
                                        <div class="controls">
                                            <input type="text" id="from_email" name="from_email" value="admin@[Domain]" placeholder="" class="span10">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Reply Email</label>
                                        <div class="controls">
                                            <input type="text" id="reply_email" name="reply_email" value="reply@[Domain]" placeholder="" class="span10">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Bounce Email</label>
                                        <div class="controls">
                                            <input type="text" id="bounce_email" name="bounce_email" value="return" placeholder="" class="span10">
                                            <span class="help-inline"><i href='#bounceemail-info' class='icon-info-sign icon-white' title='Infos' role='button' data-toggle='modal' style='cursor: pointer;' onclick=''></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput" >Return Path</label>
                                        <div class="controls">
                                            <input type="text" id="return_path" name="return_path" placeholder="" class="span10">
                                            <span class="help-inline"><i href='#returnpath-info' class='icon-info-sign icon-white' title='Infos' role='button' data-toggle='modal' style='cursor: pointer;' onclick=''></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Received</label>
                                        <div class="controls">
                                            <input type="text" id="received" name="received" placeholder="" class="span10">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">X-Mailer</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="xmailer" name="xmailer" class="span5">
                                                <option value='Alife12' selected="">Alife12</option>
                                                <option value='AlphaPlus'>AlphaPlus</option>
                                                <option value='AWeber 4.0'>AWeber 4.0</option>
                                                <option value='Bingo v1.0'>Bingo v1.0</option>
                                                <option value='Everest V1.6'>Everest V1.6</option>
                                                <option value='Feedball v1.0'>Feedball v1.0</option>
                                                <option value='My-Prog 2.2'>My-Prog 2.2</option>
                                                <option value='OutLow v5.9'>OutLow v5.9</option>
                                                <option value='SetupMail'>SetupMail</option>
                                                <option value='SlomoV2.3'>SlomoV2.3</option>
                                                <option value='SonicV3.1'>SonicV3.1</option>
                                                <option value='StuffMailV8.00'>StuffMailV8.00</option>
                                                <option value='SuperMax'>SuperMax</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Header Number</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="header_nbr" name="header_nbr" class="span5" onchange="">
                                                <option value="0" selected>Header 0</option>
                                                <option value="1" >Header 1</option>
                                                <option value="2" >Header 2</option>
                                                <option value="3" >Header 3</option>
                                                <option value="4" >Header 4</option>
                                                <option value="5" >Header 5</option>
                                                <option value="6" >Header 6</option>
                                                <option value="7" >Header 7</option>
                                                <option value="8" >Header 8</option>
                                            </select>
                                            <span class="help-inline"><a href="#headerformat-info" role="button" class="btn btn-primary" data-toggle="modal" onclick=""><i class="icon-envelope-alt icon-white"></i>&nbsp;Header Format</a></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Header</label>
                                        <div class="controls">
                                            <textarea class="span12" rows="10" id="header_format" name="header_format">
Subject: [Subject]
From: [FromName] <[FromEmail]>
Reply-to: <[ReplyEmail]>
To: [To]
Content-Type: text/html
Date: [SMTPDate]
                                            </textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Server Creative</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="server_body" name="server_body" class="span10" onchange="show_domains();">
                                                <option value="0">Select here..</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="select_server(<?php echo $id_server;?>);select_domains(<?php echo $id_server;?>);"></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Domain Creative</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="domain_body" name="domain_body" class="span10">
                                                <option value="0">Select here..</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_domains();"></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Redirect Type</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="redirect_type" name="redirect_type" class="span5">
                                                <option value="0">Normal Link</option>
                                                <option value="1">Short Link</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Include Open Tracker</label>
                                        <div class="controls">
                                            <label class="radio inline">
                                                <input type="radio" name="open_tracker" id="open_tracker_yes" value="1" onclick="" checked="">Yes
                                            </label> 
                                            <label class="radio inline">
                                                    <input type="radio" name="open_tracker" id="open_tracker_no" value="0" onclick="" >No
                                            </label>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Message Body Type</label>
                                        <div class="controls">
                                            <label class="radio inline">
                                                <input type="radio" name="body_type" id="textbody" value="0" onclick="body1();">Text
                                            </label> 
                                            <label class="radio inline">
                                                    <input type="radio" name="body_type" id="htmlbody" value="1" onclick="body2();" checked="">Html
                                            </label> 
                                            <label class="radio inline">
                                                    <input type="radio" name="body_type" id="multipartbody" value="2" onclick="body3();">Multipart (text/html)
                                            </label>
                                        </div>
                                    </div>
                                    <div class="control-group" id="source_text">
                                            <textarea class="span12" rows="15" id="text_body"  name="text_body">Text Here
                                            </textarea>
                                    </div>
                                    <div class="control-group" id="source_html">
                                        <textarea class="span12" rows="25" id="html_body" name="html_body">
<html>
<body>                
<center>                  
<a href="http://[Domain]/app/redirection/[OfferPage]" style="text-decoration: none;">
<font SIZE="4" color="#70ADEF">
Title Here
</font></a>
<br><br>
<a href="http://[Domain]/app/redirection/[OfferPage]">
<img src="http://[Domain]/app/image/6846.png"/></a>
<br>
<a href="http://[Domain]/app/redirection/[OfferUnsub]">
<img src="http://[Domain]/app/image/646869.png"/></a>
<br>
<a href="http://[Domain]/app/redirection/[ServerUnsub]">
<img src="http://[Domain]/app/image/unsubcampany.png"/></a>
</center>
</body>
</html>
                                        </textarea>
                                    </div>
                                    <div class="control-group">
                                        <a href="#modal-preview" role="button" class="btn btn-primary" data-toggle="modal" onclick="preview();"><i class="icon-picture icon-white"></i>&nbsp;Preview</a>
                                        <button class="btn btn-primary" type="button" id="advanced_setting_btn" onclick=""><i class="icon-cog icon-white"></i>&nbsp;Advanced Setting</button>
                                    </div>
                                    <div class="control-group">
                                    </div>
                                    <div id="advanced_setting_panel">
                                    <div class="control-group">
                                        <label class="control-label">Additional Negative</label>
                                        <div class="controls">
                                            <label class="radio inline">
                                                <input type="radio" name="additional_negative_active" id="" value="0" onclick="" checked="">No
                                            </label>
                                            <label class="radio inline">
                                                    <input type="radio" name="additional_negative_active" id="" value="1" onclick="">Yes
                                            </label>
                                        </div>
                                    </div>
                                        <div class="control-group">
                                        <label class="control-label">Repeat</label>
                                        <div class="controls">
                                            <input type="text" id="repeat_negative" name="repeat_negative" placeholder="" value="1" class="span3">
                                            <button class="btn btn-danger" type="button" id="" onclick="calculate_negative_size();"><i class="icon-retweet icon-white"></i>&nbsp;calculate Size</button>
                                            <span class="help-inline" id="size_result"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Negative</label>
                                        <div class="controls">
                                            <textarea class="span12" rows="10" id="additional_negative" name="additional_negative"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Test Multiple</label>
                                        <div class="controls">
                                            <label class="radio inline">
                                                <input type="radio" name="multiple_type" id="" value="0" onclick="" checked="">None
                                            </label> 
                                            <label class="radio inline">
                                                    <input type="radio" name="multiple_type" id="" value="1" onclick="">From Domain
                                            </label> 
                                            <label class="radio inline">
                                                    <input type="radio" name="multiple_type" id="" value="2" onclick="">Return Path
                                            </label>
                                            <label class="radio inline">
                                                    <input type="radio" name="multiple_type" id="" value="3" onclick="">Domain Creative
                                            </label>
                                            <label class="radio inline">
                                                    <input type="radio" name="multiple_type" id="" value="4" onclick="">Body
                                            </label>
											<label class="radio inline">
                                                    <input type="radio" name="multiple_type" id="" value="5" onclick="">Group ID
                                            </label>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Values</label>
                                        <div class="controls">
                                            <textarea class="span12" rows="10" id="domain_multiple" name="multiple_values"></textarea>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Send Multiple</label>
                                        <div class="controls">
                                            <label class="radio inline">
                                                <input type="radio" name="send_multiple_type" id="" value="0" onclick="" checked="">None
                                            </label> 
                                            <label class="radio inline">
                                                    <input type="radio" name="send_multiple_type" id="" value="1" onclick="">From Domain
                                            </label> 
                                            <label class="radio inline">
                                                    <input type="radio" name="send_multiple_type" id="" value="2" onclick="">Return Path
                                            </label>
                                            <label class="radio inline">
                                                    <input type="radio" name="send_multiple_type" id="" value="3" onclick="">Domain Creative
                                            </label>
											<label class="radio inline">
                                                    <input type="radio" name="send_multiple_type" id="" value="5" onclick="">Group ID
                                            </label>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Rotate per</label>
                                        <div class="controls">
                                            <input type="text" id="" name="send_rotate_values" placeholder="" class="span3">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Values</label>
                                        <div class="controls">
                                            <textarea class="span12" rows="10" id="" name="send_multiple_values"></textarea>
                                        </div>
                                    </div>
									<div class="control-group">
                                        <label class="control-label">Astuce Tag</label>
                                        <div class="controls">
                                            <label class="radio inline">
                                                <input type="radio" name="astuce_tag_active" id="astuce_tag_active_no" value="0" onclick="" checked=""><span class='label label-important'>No</span>
                                            </label>
                                            <label class="radio inline">
                                                    <input type="radio" name="astuce_tag_active" id="astuce_tag_active_yes" value="1" onclick=""><span class='label label-success'>Yes</span>
                                            </label>
                                        </div>
                                    </div>
									<div class="control-group">
                                        <label class="control-label">Email Addresses</label>
                                        <div class="controls">
											<textarea class="span12" rows="10" id="astuce_tag_email_address" name="astuce_tag_email_address"></textarea>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.content-->
                </div><!--/.span6-->
<!-- **************************************************************************** -->
                <div class="span6">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>MTAs / IPs / Domains <span class="refresh"><i href='#create-vmta' class='icon-pencil icon-white' title='Create VMTA' role='button' data-toggle='modal' style='cursor: pointer;' onclick='show_ips();'></i>&nbsp;&nbsp;<i href='#confirm-reset-vmtas' class='icon-remove-circle icon-white' title='Reset all VMTAs' role='button' data-toggle='modal' style='cursor: pointer;' onclick=''></i>&nbsp;&nbsp;<i href='#search-vmtas' class='icon-search icon-white' title='Search' role='button' data-toggle='modal' style='cursor: pointer;' onclick=''></i>&nbsp;&nbsp;<i href='#edit-vmtas' class='icon-edit icon-white' title='Edit' role='button' data-toggle='modal' style='cursor: pointer;' onclick='show_vmtas_editable();'></i>&nbsp;&nbsp;<i class="icon-hand-up icon-white" title="Unselect All" style="cursor: pointer;" onclick="vmta_unselect();"></i>&nbsp;&nbsp;<i class="icon-refresh icon-white" title="Refresh" style="cursor: pointer;" onclick="show_vmtas();vmta_select_count();"></i></span></h3></div>
                            <div class="module-body">
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <select tabindex="1" data-placeholder="Select here.." id="vmta" name="vmtas[]" size="10" multiple class="span12" onchange="vmta_select_count();">
                                        </select>
                                    </div>
                                </div>
                                <font size="1" id="vmta_count">No Vmta Selected</font>
                            </div>
                        </div>
                    </div><!--/.content-->
                </div><!--/.span6-->
<!-- **************************************************************************** -->
                <div class="span6">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Offer & Creative</h3></div>
                            <div class="module-body">
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Sponsor</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="sponsor" name="sponsor" class="span10" onchange="show_offers();">
                                                <option value="0">Select here..</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_sponsors();show_offers();"></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Offer</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="offer" name="offer" class="span10">
                                                <option value="0">Select here..</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_offers();"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.content-->
                </div><!--/.span6-->
<!-- **************************************************************************** -->
                <div class="span6">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Data Lists <i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_data_lists();"></i></h3></div>
                            <div class="module-body">
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">News</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="news" name="news" class="span10" onchange="show_data_lists();">
                                                <option value="0">Select here..</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_news();show_data_lists();"></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">ISP</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="isp" name="isp" class="span10" onchange="show_data_lists();">
                                                <option value="0">Select here..</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_isps();show_data_lists();"></i></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Data Lists</label>
                                        <div class="controls" id="data-list">
                                            <label class='radio'><span class='label label-important'>No Data Selected</span></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.content-->
                </div><!--/.span6-->
<!-- **************************************************************************** -->
                <div class="span6">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Send & Test Setting</h3></div>
                            <div class="module-body">
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Test Emails To</label>
                                        <div class="controls">
                                            <input type="text" id="test_emails_to" name="test_emails_to" placeholder="Email1;Email2;Email3..." class="span12">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Test Period</label>
                                        <div class="controls">
                                            <input type="text" id="test_period" name="test_period" placeholder="" class="span5">
                                            <span class="help-inline"></span>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">X-Delay</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="xdelay" name="xdelay" class="span5">
                                                <option value='1'selected>1</option>
                                                <option value='5000'>5000</option>
                                                <option value='10000'>10000</option>
                                                <option value='15000'>15000</option>
                                                <option value='20000'>20000</option>
                                                <option value='30000'>30000</option>
                                                <option value='50000'>50000</option>
                                                <option value='70000'>70000</option>
                                                <option value='100000'>100000</option>
                                                <option value='150000'>150000</option>
                                                <option value='300000'>300000</option>
                                                <option value='400000'>400000</option>
                                                <option value='500000'>500000</option>
                                                <option value='800000'>800000</option>
                                                <option value='1000000'>1000000</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Change IP</label>
                                        <div class="controls">
                                            <input type="text" id="change_ip" name="change_ip" placeholder="" class="span5">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Data From</label>
                                        <div class="controls">
                                            <input type="text" id="data_from" name="data_from" placeholder="" class="span4">
                                            <span class="help-inline">Data Count&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <input type="text" id="data_count" name="data_count" placeholder="" class="span4">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Output</label>
                                        <div class="controls">
                                            <iframe class="span12" name="frame"  id="frame" scrolling="yes" frameborder="0" style="height: 495px; "></iframe>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <button class="btn btn-info" type="button" onclick="test_auto();"><i class="icon-bookmark icon-white"></i>&nbsp;Test Auto</button>
                                        <button class="btn btn-warning" type="button" onclick="test_campaign();"><i class="icon-share icon-white"></i>&nbsp;Test Campaign</button>
                                        <button class="btn btn-success" type="button" onclick="send_campaign();"><i class="icon-envelope icon-white"></i>&nbsp;Send Campaign</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.content-->
                </div><!--/.span6-->
                
            </form>
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
    <!-- Modal -->
    <div id="modal-preview" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">Preview</h3>
      </div>
        <div class="alert" id="message-warning-custom-2">
            <button type="button" class="close" onclick="close_message_warning_custom_2();">×</button>
            <strong>Warning!</strong> <span id="message-custom-2">Please select the domain of creative</span>
        </div>
      <div class="modal-body" id="preview">
        <p></p>
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
      </div>
    </div>
    
    <!-- Modal -->
    <div id="subject-info" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Subject infos</h3>
        </div>
        <div class="modal-body">
            <p><strong>[Domain]</strong> Return RDNS</p>
            <p><strong>[Ip]</strong> Return IP of VMTA</p>
            <p><strong>[Vmta]</strong> Show VMTA name</p>
            <p><strong>[FromEmail]</strong> Return From Email of sender</p>
            <p><strong>[ReturnPath]</strong> Return ReturnPath</p>
            <p><strong>[To]</strong> Return recipient Email</p>
            <p><strong>[Fname]</strong> Return ID of recipient Email</p>
            <p><strong>[Random(param1,param2)]</strong> Return random value<br> param1: type (ex. A or a or AN or aN or N) A=uppercase  a=lowercase  N=digital<br> param2: size (ex. 5) </p>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
     </div>
    
    <!-- Modal -->
    <div id="bounceemail-info" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Bounce Email infos</h3>
        </div>
        <div class="modal-body">
            <p><strong>[Dynamique]</strong> Return dynamique value contains the recipient domain + unique ID + recipient ID (ex. user@hotmail.com become hotmail.com.10513724975758.user ) </p>
            <p><strong>[DomainAbstract]</strong> Return abstract domain name of host name </p>
            <p><strong>[CampaignID]</strong> Return Campaign ID </p>
            <p><strong>[Random(param1,param2)]</strong> Return random value<br> param1: type (ex. A or a or AN or aN or N) A=uppercase  a=lowercase  N=digital<br> param2: size (ex. 5) </p>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
     </div>
    
    <!-- Modal -->
    <div id="returnpath-info" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Return Path infos</h3>
        </div>
        <div class="modal-body">
            <p><strong>[Empty]</strong> Empty Return Path</p>
            <p><strong>[ListID].[UserID].[Domain]</strong> Variable Return Path (SpamTraps)</p>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
     </div>
    
    <!-- Modal -->
    <div id="headerformat-info" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Header Format</h3>
        </div>
        <div class="modal-body">
            <p><strong>Subject:</strong> [Subject]</p>
            <p><strong>From:</strong> [FromName] <[FromEmail]> or [From]</p>
            <p><strong>To:</strong> [To]</p>
            <p><strong>Reply-To:</strong> <[ReplyEmail]></p>
            <p><strong>Date:</strong> [SMTPDate]</p>
            <p><strong>MIME-Version:</strong> 1.0</p>
            <p><strong>X-Originating-IP:</strong> [Ip]</p>
            <p><strong>Message-ID:</strong> [UniqueID].[CampaignID].[ListID].[UserID].[Random(AN,20)]@[Domain]</p>
            <p><strong>Content-Type:</strong> text/html</p>
            <p><strong>Content-Type:</strong> text/plain;</p>
            <p><strong>Content-Type:</strong> text/html; charset="iso-8859-1"</p>
            <p><strong>Content-Type:</strong> text/html; charset=us-ascii;</p>
            <p><strong>Content-Transfer-Encoding:</strong> 7bit</p>
            <p><strong>Content-Transfer-Encoding:</strong> quoted-printable</p>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
     </div>

	<!-- Modal -->
    <div id="create-vmta" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Create VMTA </h3>
        </div>
		<div class="modal-body">
			<div class="alert" id="message-warning-vmta">
				<button type="button" class="close" onclick="close_message_warning_vmta();">×</button>
				<strong>Warning!</strong> Please verify your data
			</div>
			<div class="alert" id="message-warning-custom-vmta">
				<button type="button" class="close" onclick="close_message_warning_custom_vmta();">×</button>
				<strong>Warning!</strong> <span id="message-custom-vmta"></span>
			</div>
			<div class="alert alert-error" id="message-error-vmta">
				<button type="button" class="close" onclick="close_message_error_vmta();">×</button>
				<strong>Error!</strong> There was an error while executing your request
			</div>
			<div class="alert alert-success" id="message-success-vmta">
				<button type="button" class="close" onclick="close_message_success_vmta();">×</button>
				<strong>Done!</strong> VMTA has been created successfully
			</div>
			<form class="form-horizontal row-fluid" name="" method="POST" action=""> 
				<div class="control-group">
					<label class="control-label" for="basicinput">VMTA Name</label>
					<div class="controls">
						<input type="text" id="name_vmta" name="name_vmta" placeholder="No spaces or special characters" class="span8">
						<span class="help-inline"><button class="btn btn-success" type="button" name="" onclick="generate_name();"><i class="icon-retweet icon-white"></i>&nbsp;Generate</button></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="basicinput">IP Addresses</label>
					<div class="controls">
						<select tabindex="1" data-placeholder="Select here.." id="address_ip_vmta" name="address_ip_vmta[]" size="10" multiple class="span8" style="float: left;" onchange="ip_select_count();"></select>
						<span class="" style="float: left;">&nbsp;&nbsp;&nbsp;<i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_ips();"></i></span><br/>
						&nbsp;&nbsp;&nbsp;<font size="1" id="ip_count">No Vmta Selected</font>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="basicinput">Host Name</label>
					<div class="controls">
						<input type="text" id="host_name_vmta" name="host_name_vmta" placeholder="" class="span8">
						<span class="help-inline"></span>
					</div>
				</div>
			</form>
		</div>
        <div class="modal-footer">
		  <span class="processing" id="processing_vmta" style="float: left; margin-bottom: -20px;"></span> 
		  <button class="btn btn-primary" type="button" name="submit" onclick="add_vmta();"><i class="icon-pencil icon-white"></i>&nbsp;Create VMTA</button>
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
     </div>
	 <!-- Modal -->
	 <div id="confirm-reset-vmtas" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		  <h3 id="myModalLabel">Confirmation</h3>
		</div>
		<div class="modal-body">
		  <p>You're about to Reset all Virtual MTAs in this Server</p>
		</div>
		<div class="modal-footer">
		  <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
		  <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="reset_vmtas()">Confirm</button>
		</div>
	  </div>
    
    <!-- Modal -->
    <div id="edit-vmtas" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Edit VMTAs</h3>
        </div>
        <div class="modal-body">
            <p>
                <div class="control-group">
                    <label class="control-label">Sort by</label>
                    <div class="controls">
                        <label class="checkbox inline"><input type="checkbox" id='edit-vmta-name' name="vmta_option" value="0" onclick="show_vmtas_editable();" checked>Name</label>
                        <label class="checkbox inline"><input type="checkbox" id='edit-vmta-ip' name="vmta_option" value="1" onclick="show_vmtas_editable();" checked>IP</label>
                        <label class="checkbox inline"><input type="checkbox" id='edit-vmta-host' name="vmta_option" value="2" onclick="show_vmtas_editable();" checked>Host Name</label>
                    </div>
                </div>

            </p>
          <p><textarea class="span5" rows="10" id="edit_vmtas_area"></textarea></p>
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
     </div>
    
    <!-- Modal -->
    <div id="search-vmtas" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Search VMTAs</h3>
        </div>
        <div class="modal-body">
            <p>
                <div class="control-group">
                    <label class="control-label">Search by</label>
                    <div class="controls">
                        <label class="checkbox inline"><input type="checkbox" id='search-vmta-name' name="vmta_option" value="0" onclick="" checked>Name</label>
                        <label class="checkbox inline"><input type="checkbox" id='search-vmta-ip' name="vmta_option" value="1" onclick="" checked>IP</label>
                        <label class="checkbox inline"><input type="checkbox" id='search-vmta-host' name="vmta_option" value="2" onclick="" checked>Host Name</label>
                    </div>
                </div>

            </p>
          <p><textarea class="span5" rows="10" id="search_vmtas_area"></textarea></p>
          <font size="1" id="vmta_count">(*) Each value in a row</font>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="search_vmtas();">Search</button>
        </div>
     </div>

    
</body>
</html>

<?php
    if(isset($_GET['edit_campaign'])){
        echo "<script>edit_campaign(".$_GET['edit_campaign'].")</script>";
        //echo "<script>alert(".$_GET['edit_campaign'].")</script>";
    }
?>