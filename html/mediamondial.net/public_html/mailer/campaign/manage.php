﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage PMTA</title>
    <link type="text/css" href="/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="/static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="/static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="/static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="/static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="/static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>
</head>
<body>
	<div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <?php include("../inc.top_menu.php");  ?>
			</div>
        </div><!-- /navbar-inner -->
	</div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <ul class="widget widget-menu unstyled">
                            <li><a href="#"><i class="menu-icon icon-dashboard"></i>Dashboard</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/campaign/send.php"><i class="menu-icon icon-envelope"></i>Send Campaign</a></li>
                            <li><a href="/campaign/show.php"><i class="menu-icon icon-cogs"></i>Manage Campaigns</a></li>
                        </ul><!--/.widget-nav-->

                        <ul class="widget widget-menu unstyled">
                            <li><a href="/pmta/monitor.php"><i class="menu-icon icon-calendar"></i>PowerMTA Monitoring</a></li>
                            <li class="active"><a href="/pmta/manage.php"><i class="menu-icon icon-cogs"></i>Manage PowerMTA</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/image/upload.php"><i class="menu-icon icon-upload"></i>Upload Images</a></li>
                            <li><a href="/image/show.php"><i class="menu-icon icon-cogs"></i>Manage Images</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li>
                                <a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-bullhorn"></i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>Offers</a>
                                <ul id="togglePages" class="collapse unstyled">
                                    <li><a href="/offer/show.php"><i class="icon-list"></i>Show Offers</a></li>
                                    <li><a href="/offer/links.php"><i class="icon-link"></i>Links & Redirect</a></li>
                                </ul>
                            </li>
                            <li><a href="/sponsor/show.php"><i class="menu-icon icon-certificate"></i>Sponsors</a></li>
                            <li><a href="/server/show.php"><i class="menu-icon icon-hdd"></i>Servers</a></li>
                        </ul>

                        <ul class="widget widget-menu unstyled">
                            <li>
                                <a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-user"></i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>My Account</a>
                                <ul id="togglePages" class="collapse unstyled">
                                    <li><a href="#"><i class="icon-edit"></i>Edit Profile</a></li>
                                    <li><a href="#"><i class="icon-cog"></i>Account Settings</a></li>
                                </ul>
                            </li>
                            <li><a href="/account/logout.php"><i class="menu-icon icon-signout"></i>Logout</a></li>
                        </ul>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Manage PMTA <div class="processing" id="processing"></div></h3></div>
                            
                            <div class="module-body">
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Delete Queue</label>
                                        <div class="controls">
                                            <input type="text" id="isp" name="isp" placeholder="Domain here..." class="span3">
                                            <input type="text" id="vmta" name="vmta" placeholder="VMTA here... (*)" class="span3">
                                            <button class="btn btn-warning" type="button" onclick="delete_queue_command();"><i class="icon-remove-sign icon-white"></i>&nbsp;Delete</button>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Delete JobId</label>
                                        <div class="controls">
                                            <input type="text" id="jobid" name="jobid" placeholder="Jobid here..." class="span3">
                                            <button class="btn btn-warning" type="button" onclick="delete_jobid_command();"><i class="icon-remove-sign icon-white"></i>&nbsp;Delete</button>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Command</label>
                                        <div class="controls">
                                            <input type="text" id="command" name="command" placeholder="Type command here..." style="width: 220px;">
                                            <button class="btn btn-primary" type="button" onclick="run_command();"><i class="icon-play icon-white"></i>&nbsp;Run Command</button>
                                            <button class="btn btn-success" type="button" onclick="restart_command();"><i class="icon-repeat icon-white"></i>&nbsp;Restart</button>
                                            <button class="btn btn-success" type="button" onclick="reset_command();"><i class="icon-retweet icon-white"></i>&nbsp;Reset</button>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">PMTA Output</label>
                                        <div class="controls" id="pmta_report">
                                            <!--div class="span10" rows="10"  style="width: 86%;"></div-->
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <span class='label label-info'>Commands list</span>
                                        <ul>
                                            <li>restart</li>
                                            <li>status</li>
                                            <li>clear dnscache domainname</li>
                                            <li>check mfrom [--tcp] [--dumppackets] mailfrom ip</li>
                                            <li>check pra [--tcp] [--dumppackets] pra ip</li>
                                            <li>delete [--dsn] [--accounting] [--queue=domain[/vmta]] [--orig=addr] [--rcpt=addr] [--jobId=id] [--envId=id]</li>
                                            <li>list [--queue=domain[/vmta]] [--orig=addr] [--rcpt=addr] [--jobId=id] [--envId=id] [--maxitems=n] [--pause]</li>
                                            <li>pause queue domain[/vmta]</li>
                                            <li>reload</li>
                                            <li>reset counters</li>
                                            <li>reset status</li>
                                            <li>resolve [--tcp] [--connect] [--dumppackets] [--interactive] [--source=[host,]ip] domainname</li>
                                            <li>resume queue domain[/vmta]</li>
                                            <li>rotate acct [file]</li>
                                            <li>rotate log</li>
                                            <li>schedule domain[/vmta]</li>
                                            <li>set queue --mode={normal|backoff} domain[/vmta]</li>
                                            <li>show domains [--vmta=name] [--connected={yes|no}] [--maxitems=n] [--errors] [--sort={name|rcpt|size}] [name]</li>
                                            <li>show jobs</li>
                                            <li>show license</li>
                                            <li>show queues [--connected={yes|no}] [--paused={yes|no}] [--mode={normal|backoff}] [--maxitems=n] [--errors] [--sort={name|rcpt|size}] [domain[/vmta]]</li>
                                            <li>show status</li>
                                            <li>show settings domain[/vmta]</li>
                                            <li>show topdomains [--vmta=name] [--connected={yes|no}] [--maxitems=n] [--errors]</li>
                                            <li>show topqueues [--connected={yes|no}] [--paused={yes|no}] [--mode={normal|backoff}] [--maxitems=n] [--errors] [domain[/vmta]]</li>
                                            <li>show version</li>
                                            <li>show vmtas</li>
                                            <li>trace [--logdata] [--logresolution] domain[/vmta]</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>