﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $name_server=$_SESSION['name-server'];
    $id_mailer=$_SESSION['id-mailer'];
    $username_mailer=$_SESSION['username-mailer'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show Campaign Stats</title>
    <link type="text/css" href="..//static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="..//static/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="..//static/css/theme.css" rel="stylesheet">
    <link type="text/css" href="..//static/images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="..//static/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="..//static/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="..//static/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="..//static/scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            refresh_stats_campaign(<?php echo $_GET["id_campaign"];?>);
        });
    </script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse"><i class="icon-reorder shaded"></i></a>
                <!--a class="brand" href="/"><img src="/static/images/logo.jpg"/></a-->
                <a class="brand" href="/"><img class="logo" src="..//static/images/logo.png"></a>
                
                <div class="nav-collapse collapse navbar-inverse-collapse">
                    <ul class="nav nav-icons">
                        <li><a href="#"><i><?php echo $name_server; ?></i></a></li>
                    </ul>
                    
                    <ul class="nav pull-right">
                        <li><a href="/campaign/send.php" >Send Campaign</a></li>
                        <li><a href="/campaign/show.php" >Show Campaigns</a></li>
                        <li><a href="/pmta/manage.php" >Manage PMTA</a></li>
                        <li><a href="/pmta/monitor.php" >Monitoring PMTA</a></li>
                        <li><a href="/image/upload.php" >Upload Images</a></li>
                        <li><a href="/image/show.php" >Show Images</a></li>
                        <li class="nav-user dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="..//static/images/user.png" class="nav-avatar" /><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                    <li><a href="#"><?php echo $username_mailer; ?></a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Account Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/account/logout.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        <ul class="widget widget-menu unstyled">
                            <li><a href="#"><i class="menu-icon icon-dashboard"></i>Dashboard</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/campaign/send.php"><i class="menu-icon icon-envelope"></i>Send Campaign</a></li>
                            <li><a href="/campaign/show.php"><i class="menu-icon icon-cogs"></i>Manage Campaigns</a></li>
                        </ul><!--/.widget-nav-->

                        <ul class="widget widget-menu unstyled">
                            <li><a href="/pmta/monitor.php"><i class="menu-icon icon-calendar"></i>PowerMTA Monitoring</a></li>
                            <li class="active"><a href="/pmta/manage.php"><i class="menu-icon icon-cogs"></i>Manage PowerMTA</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/image/upload.php"><i class="menu-icon icon-upload"></i>Upload Images</a></li>
                            <li><a href="/image/show.php"><i class="menu-icon icon-cogs"></i>Manage Images</a></li>
                        </ul><!--/.widget-nav-->

                        <ul class="widget widget-menu unstyled">
                            <li>
                                <a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-user"></i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>My Account</a>
                                <ul id="togglePages" class="collapse unstyled">
                                    <li><a href="#"><i class="icon-edit"></i>Edit Profile</a></li>
                                    <li><a href="#"><i class="icon-cog"></i>Account Settings</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="menu-icon icon-signout"></i>Logout</a></li>
                        </ul>
                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Show Campaign Stats <?php echo $_GET["id_campaign"];?> &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-refresh icon-white" title="Refresh" style="cursor: pointer;" onclick="refresh_stats_campaign(<?php echo $_GET["id_campaign"];?>);"></i> <div class="processing" id="processing"></div></h3></div>
                        </div>
                        <div class="alert alert-error" id="message-error">
                            <button type="button" class="close" onclick="close_message_error();">×</button>
                            <strong>Error!</strong> There was an error while executing your request
                        </div>
                        <div class="btn-controls">
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-list-alt"></i><b id="total-selected">-</b><p class="text-muted">Selected</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-share"></i><b id="total-processed">-</b><p class="text-muted">Processed</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-check"></i><b id="total-delivered">-</b><p class="text-muted">Delivered</p></a>
                            </div>
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-remove-circle"></i><b id="total-bounces">-</b><p class="text-muted">Total Bounce</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-ban-circle"></i><b id="total-hardbounce">-</b><p class="text-muted">Hard Bounce</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-warning-sign"></i><b id="total-softbounce">-</b><p class="text-muted">Soft Bounce</p></a>
                            </div>
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-folder-open"></i><b id="total-opens">-</b><p class="text-muted">Opens</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-hand-up"></i><b id="total-clicks">-</b><p class="text-muted">Clicks</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-money"></i><b id="total-earnings">-</b><p class="text-muted">Earnings</p></a>
                            </div>
                            <div class="btn-box-row row-fluid">
                                <ul class="widget widget-usage unstyled span4">
                                    <li>
                                        <p><strong>Open</strong> <span class="pull-right small muted" id="opens-rate">-</span></p>
                                        <div class="progress tight">
                                            <div class="bar bar-warning" id="opens-rate-bar" style="width: 0%;"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <p><strong>Click</strong> <span class="pull-right small muted" id="clicks-rate">-</span></p>
                                        <div class="progress tight">
                                            <div class="bar bar-success" id="clicks-rate-bar" style="width: 0%;"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <p><strong>Bounce</strong> <span class="pull-right small muted" id="bounces-rate">-</span></p>
                                        <div class="progress tight">
                                            <div class="bar bar-danger" id="bounces-rate-bar" style="width: 0%;"></div>
                                            
                                        </div>
                                    </li>
                                    <li>
                                        <p><strong>Not Open</strong> <span class="pull-right small muted" id="not-opens-rate">-</span></p>
                                        <div class="progress tight">
                                            <div class="bar" id="not-opens-rate-bar" style="width: 0%;"></div>
                                        </div>
                                    </li>
                                </ul>
                                <ul class="widget widget-usage unstyled span4">
                                    <li>
                                        <p><strong>[OfferPage]</strong> <span class="pull-right small muted" id="clicks-offerpage-rate">-</span></p>
                                        <div class="progress tight">
                                            <div class="bar bar-success" id="clicks-offerpage-rate-bar" style="width: 0%;"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <p><strong>[OfferUnsub]</strong> <span class="pull-right small muted" id="clicks-offerunsub-rate">-</span></p>
                                        <div class="progress tight">
                                            <div class="bar bar-warning" id="clicks-offerunsub-rate-bar" style="width: 0%;"></div>
                                        </div>
                                    </li>
                                    <li>
                                        <p><strong>[ServerUnsub]</strong> <span class="pull-right small muted" id="clicks-serverunsub-rate">-</span></p>
                                        <div class="progress tight">
                                            <div class="bar bar-danger" id="clicks-serverunsub-rate-bar" style="width: 0%;"></div>
                                            
                                        </div>
                                    </li>
                                </ul>
                                <div class="span4">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <a href="#" class="btn-box small span4"><b>[OfferPage]</b><span class="small muted" id="total-clicks-offerpage">-</span></a>
                                            <a href="#" class="btn-box small span4"><b>[OfferUnsub]</b><span class="small muted" id="total-clicks-offerunsub">-</span></a>
                                            <a href="#" class="btn-box small span4"><b>[ServerUnsub]</b><span class="small muted" id="total-clicks-serverunsub">-</span></a>
                                        </div>
                                    </div>
                                </div>
                                
                                <!--div class="span8">
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <a href="#" class="btn-box small span4"><i class="icon-envelope"></i><b>Messages</b><span class="small muted">30 00</span><br><span class="small muted">20%</span></a>
                                            <a href="#" class="btn-box small span4"><i class="icon-group"></i><b>Clients</b><span class="small muted">30 00</span></a>
                                            <a href="#" class="btn-box small span4"><i class="icon-exchange"></i><b>Expenses</b><span class="small muted">30 00</span></a>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <div class="span12">
                                            <a href="#" class="btn-box small span4"><i class="icon-save"></i><b>Total Sales</b><span class="small muted">30 00</span></a>
                                            <a href="#" class="btn-box small span4"><i class="icon-bullhorn"></i><b>Social Feed</b><span class="small muted">30 00</span></a>
                                            <a href="#" class="btn-box small span4"><i class="icon-sort-down"></i><b>BounceRate</b><span class="small muted">30 00</span></a>
                                        </div>
                                    </div>
                                </div-->
                            </div>
                        </div>
                        <!--/#btn-controls-->
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>