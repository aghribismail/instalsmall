<?php
    include_once '../scripts/bd.php';
    include('../scripts/bdd.php');
    
    if($_GET['id_campaign']!="0" && $_GET["id_user"]!="0")
    {
        $date_open  =   date('Y-m-d h:i:s');
        $ip         =   $_SERVER['REMOTE_ADDR'];
        bd::query("INSERT INTO track_open VALUES(NULL,'{$_GET["id_campaign"]}','{$_GET["id_list"]}','{$_GET["id_user"]}','$ip','0','$date_open')");
    
        
        if( ($_GET["id_list"] > 0) && ($_GET["id_list"] != 9) && ($_GET["id_user"] > 0))
        {
            $id_list    =   $_GET["id_list"];
            $id_user    =   $_GET["id_user"];
            $id_campaign=   $_GET["id_campaign"];

            //0- Get Offer Infos :
             $sqlOfferInfos  =
            "
                SELECT  id_offer 
                FROM    campaign 
                WHERE   id  =   ?
            ";
            $cmdOfferInfos  =   $bdd->prepare($sqlOfferInfos);
            $cmdOfferInfos->execute(array($id_campaign));
            $res            =   $cmdOfferInfos->fetch();
            $id_offer      =    $res['id_offer'];
            $cmdOfferInfos->closeCursor();
            
            //1- Get List Infos :
            $sqlTableInfos  =
            "
                SELECT  data_table 
                FROM    data_list 
                WHERE   id  =   ?
            ";

            $cmdTableInfos  =   $bdd->prepare($sqlTableInfos);
            $cmdTableInfos->execute(array($id_list));
            $res            =   $cmdTableInfos->fetch();
            $tableName      =   $res['data_table'];
            $cmdTableInfos->closeCursor();



            //2- Get Email Infos :
            $sqlEmailInfos  =
            "
                SELECT  *
                FROM    $tableName
                WHERE   id  =   ?
            ";
            $cmdEmailInfos  =   $bdd->prepare($sqlEmailInfos);
            $cmdEmailInfos->execute(array($id_user));
            $user            =   $cmdEmailInfos->fetch();
            $cmdEmailInfos->closeCursor();


            //3- Get the vertical open table name :
            $sqlVerticalClickTable      =
            "
                SELECT  name_open_table
                FROM    vertical
                WHERE   id_vertical     =   (select id_vertical from offer where id = ?)
            ";
            $cmdVerticalClickTable      =   $bdd->prepare($sqlVerticalClickTable);
            $cmdVerticalClickTable->execute(array($id_offer));
            $vertical_row               =   $cmdVerticalClickTable->fetch();
            $verticalClickTableName     =   $vertical_row['name_open_table'];
            $cmdVerticalClickTable->closeCursor();


            //4- Save in the vertical table :
            if($user && !empty($verticalClickTableName))
            {
                $uniq_id        =   $user['unique_id'];
                $email_address  =   $user['email_address'];
                $email_md5      =   $user['email_md5'];

                $sqlInsertUser  =
                "
                    INSERT  INTO    $verticalClickTableName     VALUES (NULL,?,?,?);
                ";
                $cmdInsertUser  =   $bdd_data->prepare($sqlInsertUser);
                $cmdInsertUser->execute(array($uniq_id,$email_address,$email_md5));
                $cmdInsertUser->closeCursor();
            }
        }
        
    }
    

    

