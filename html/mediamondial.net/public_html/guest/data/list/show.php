﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../../account/session.php';
    include_once 'scripts.php';
    $id_guest=$_SESSION['id-guest'];
    $username_guest=$_SESSION['username-guest'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Data Lists</title>
    <link type="text/css" href="../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="../../../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="../../../css/theme.css" rel="stylesheet">
    <link type="text/css" href="../../../images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="../../../scripts/jquery-1.9.1.min.js"></script>
	<script src="../../../scripts/jquery-ui-1.10.1.custom.min.js"></script>
	<script src="../../../bootstrap/js/bootstrap.min.js"></script>
	<script src="../../../scripts/datatables/jquery.dataTables.js"></script>
        <script src="scripts.js" type="text/javascript"></script>
	<script>
            $(document).ready(function() {
                
                    select_news_2(<?php echo $_GET['id_news'];?>);
                    
                    $('.datatable-1').dataTable();
                    $('.datatable-1').find('thead th').css('width', 'auto');
                    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
                    $('.dataTables_paginate > a').wrapInner('<span />');
                    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
                    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
            } );
	</script>
</head>
<body>
    
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse"><i class="icon-reorder shaded"></i></a>
                <a class="brand" href="index.php">Mail App</a>
                
                <div class="nav-collapse collapse navbar-inverse-collapse">
                    <ul class="nav pull-right">
                        <li class="active"><a href="/app/guest/data/list/">Manage Data Lists</a></li>
                        <li class="nav-user dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../../../images/user.png" class="nav-avatar" /><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                    <li><a href="#"><?php echo $username_guest; ?></a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Account Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/app/guest/account/logout.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="#"><i class="menu-icon icon-dashboard"></i>Dashboard</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/app/guest/data/list/show.php"><i class="menu-icon icon-list"></i>Show Data Lists</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/app/guest/data/news/show.php"><i class="menu-icon icon-list"></i>Show News</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li>
                                <a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-user"></i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>My Account</a>
                                <ul id="togglePages" class="collapse unstyled">
                                    <li><a href="#"><i class="icon-edit"></i>Edit Profile</a></li>
                                    <li><a href="#"><i class="icon-cog"></i>Account Settings</a></li>
                                </ul>
                            </li>
                            <li><a href="/app/guest/account/logout.php"><i class="menu-icon icon-signout"></i>Logout</a></li>
                        </ul>

                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Show Data Lists &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-refresh icon-white" title="Refresh" style="cursor: pointer;" onclick="refresh();"></i><div class="processing" id="processing"></div></h3></div>
                            <div class="module-body">
                                <div class="form-horizontal row-fluid">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">News</label>
                                        <div class="controls">
                                            <select tabindex="1" data-placeholder="Select here.." id="news" class="span5" onchange="refresh();">
                                                <option value="0">Select here...</option>
                                            </select>
                                            <span class="help-inline"><i class="icon-refresh icon-white refresh" title="Refresh" style="cursor: pointer;" onclick="show_news();"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="module-body table">
                                <div class="alert" id="message-warning">
                                    <button type="button" class="close" onclick="close_message_warning();">×</button>
                                    <strong>Warning!</strong> Please verify your data
                                </div>
                                <div class="alert alert-error" id="message-error">
                                    <button type="button" class="close" onclick="close_message_error();">×</button>
                                    <strong>Error!</strong> There was an error while executing your request
                                </div>
                                <div class="alert alert-success" id="message-success">
                                    <button type="button" class="close" onclick="close_message_success();">×</button>
                                    <strong>Done!</strong> Data List has been updated successfully
                                </div>
                                <table id="show" cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Count</th>
                                            <th>Active</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Count</th>
                                            <th>Active</th>
                                            <th>Options</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div><!--/.module-->
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
       
</body>