﻿<!DOCTYPE html>
<html lang="en">
<?php 
    include_once '../../../account/session.php';
    include_once 'scripts.php';
    session_start();
    $id_guest=$_SESSION['id-guest'];
    $username_guest=$_SESSION['username-guest'];
?>
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Show Data List Stats</title>
    <link type="text/css" href="../../../../bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="../../../../bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="../../../../css/theme.css" rel="stylesheet">
    <link type="text/css" href="../../../../images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
    <script src="../../../../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../../../../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="../../../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../../../scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="scripts.js" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            
            var today=new Date();
            $("#from_mm").val(today.getMonth()+1);
            $("#from_dd").val(1);
            $("#from_yy").val(today.getFullYear());
            $("#to_mm").val(today.getMonth()+1);
            $("#to_dd").val(today.getDate());
            $("#to_yy").val(today.getFullYear());
            
            refresh_stats_data_list(<?php echo $_GET["id_list"];?>);
        });
    </script>
</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse"><i class="icon-reorder shaded"></i></a>
                <a class="brand" href="index.php">Mail App</a>
                
                <div class="nav-collapse collapse navbar-inverse-collapse">
                    <ul class="nav pull-right">
                        <li class="active"><a href="/app/guest/data/list/">Manage Data Lists</a></li>
                        <li class="nav-user dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="../../../../images/user.png" class="nav-avatar" /><b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                    <li><a href="#"><?php echo $username_guest; ?></a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Account Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="/app/guest/account/logout.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.nav-collapse -->
            </div>
        </div><!-- /navbar-inner -->
    </div><!-- /navbar -->

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <div class="sidebar">
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="#"><i class="menu-icon icon-dashboard"></i>Dashboard</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/app/guest/data/list/show.php"><i class="menu-icon icon-list"></i>Show Data Lists</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li><a href="/app/guest/data/news/show.php"><i class="menu-icon icon-list"></i>Show News</a></li>
                        </ul><!--/.widget-nav-->
                        
                        <ul class="widget widget-menu unstyled">
                            <li>
                                <a class="collapsed" data-toggle="collapse" href="#togglePages"><i class="menu-icon icon-user"></i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>My Account</a>
                                <ul id="togglePages" class="collapse unstyled">
                                    <li><a href="#"><i class="icon-edit"></i>Edit Profile</a></li>
                                    <li><a href="#"><i class="icon-cog"></i>Account Settings</a></li>
                                </ul>
                            </li>
                            <li><a href="/app/guest/account/logout.php"><i class="menu-icon icon-signout"></i>Logout</a></li>
                        </ul>

                    </div><!--/.sidebar-->
                </div><!--/.span3-->
                <div class="span9">
                    <div class="content">
                        <div class="module">
                            <div class="module-head"><h3>Show Data List Stats <span class="label label-info"><?php get_data_list_name($_GET["id_list"]);?></span>  &nbsp;&nbsp;&nbsp;&nbsp;<i class="icon-refresh icon-white" title="Refresh" style="cursor: pointer;" onclick="refresh_stats_data_list(<?php echo $_GET["id_list"];?>);"></i> <div class="processing" id="processing"></div></h3></div>
                        </div>
                        <div class="alert alert-error" id="message-error">
                            <button type="button" class="close" onclick="close_message_error();">×</button>
                            <strong>Error!</strong> There was an error while executing your request
                        </div>
                        <div class="module-body">
                            <div class="form-inline row-fluid">
                                <div class="control-group">
                                    <div class="controls">
                                        <label class="control-label" for="basicinput">Date From:&nbsp;&nbsp;</label>
                                        <select tabindex="1" data-placeholder="Select here.." id="from_mm" style="width: 110px;" onchange="">
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                        <select tabindex="1" data-placeholder="Select here.." id="from_dd" style="width: 55px;" onchange="">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                        <select tabindex="1" data-placeholder="Select here.." id="from_yy" style="width: 70px;" onchange="">
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <label class="control-label" for="basicinput">Date To:&nbsp;&nbsp;</label>
                                        <select tabindex="1" data-placeholder="Select here.." id="to_mm" style="width: 110px;" onchange="">
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                        <select tabindex="1" data-placeholder="Select here.." id="to_dd" style="width: 55px;" onchange="">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                            <option value="13">13</option>
                                            <option value="14">14</option>
                                            <option value="15">15</option>
                                            <option value="16">16</option>
                                            <option value="17">17</option>
                                            <option value="18">18</option>
                                            <option value="19">19</option>
                                            <option value="20">20</option>
                                            <option value="21">21</option>
                                            <option value="22">22</option>
                                            <option value="23">23</option>
                                            <option value="24">24</option>
                                            <option value="25">25</option>
                                            <option value="26">26</option>
                                            <option value="27">27</option>
                                            <option value="28">28</option>
                                            <option value="29">29</option>
                                            <option value="30">30</option>
                                            <option value="31">31</option>
                                        </select>
                                        <select tabindex="1" data-placeholder="Select here.." id="to_yy" style="width: 70px;" onchange="">
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select>
                                        <span class="help-inline"><button class="btn btn-primary" type="button" onclick="refresh_stats_data_list(<?php echo $_GET["id_list"];?>);"><i class="icon-play icon-white"></i>&nbsp;Run</button></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-controls">
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-folder-open"></i><b id="total-opens">-</b><p class="text-muted">Opens</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-hand-up"></i><b id="total-clicks">-</b><p class="text-muted">Clicks</p></a>
                                <a href="#" class="btn-box big span4"><i class="icon-money"></i><b id="total-earnings">-</b><p class="text-muted">Earnings</p></a>
                            </div>
                        </div>
                        <div class="btn-controls">
                            <div class="btn-box-row row-fluid">
                                <a href="#" class="btn-box big span4"><i class="icon-warning-sign"></i><b id="total-bounces">-</b><p class="text-muted">Hard Bounces</p></a>
                            </div>
                        </div>
                        <!--/#btn-controls-->
                    </div><!--/.content-->
                </div><!--/.span9-->
            </div>
        </div><!--/.container-->
    </div><!--/.wrapper-->

    <div class="footer">
        <div class="container">
            <b class="copyright">&copy; Mail App </b> All rights reserved.
        </div>
    </div>
    
</body>