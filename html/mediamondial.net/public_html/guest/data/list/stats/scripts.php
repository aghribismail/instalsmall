<?php
    include_once '../../../account/session.php';
    include_once '../../../../scripts/bd.php';
    session_start();
    $id_server=$_SESSION['id-server'];
    $id_mailer=$_SESSION['id-mailer'];
   
    if(isset($_GET['action'])){
     $action=$_GET['action'];
     if($action=='get_data_list_opens')get_data_list_opens();
     if($action=='get_data_list_clicks')get_data_list_clicks();
     if($action=='get_data_list_bounces')get_data_list_bounces();
     if($action=='get_data_list_earnings')get_data_list_earnings();
     /*if($action=='get_campaign_opens_rate')get_campaign_opens_rate();
     if($action=='get_campaign_clicks')get_campaign_clicks();
     if($action=='get_campaign_clicks_rate')get_campaign_clicks_rate();
     if($action=='get_campaign_bounces')get_campaign_bounces();
     if($action=='get_campaign_bounces_rate')get_campaign_bounces_rate();
     if($action=='get_campaign_clicks_offerpage')get_campaign_clicks_offerpage();
     if($action=='get_campaign_clicks_offerpage_rate')get_campaign_clicks_offerpage_rate();
     if($action=='get_campaign_clicks_offerunsub')get_campaign_clicks_offerunsub();
     if($action=='get_campaign_clicks_offerunsub_rate')get_campaign_clicks_offerunsub_rate();
     if($action=='get_campaign_clicks_serverunsub')get_campaign_clicks_serverunsub();
     if($action=='get_campaign_clicks_serverunsub_rate')get_campaign_clicks_serverunsub_rate();*/
    }
   
    function get_data_list_opens(){
        $opens="-";
        $query = bd::query("SELECT id_news FROM data_list WHERE id='{$_GET["id_data_list"]}'");
        $row=mysql_fetch_array($query);
        $id_news=$row['id_news'];
        if($_SESSION['id-news']==$id_news){
            $query=bd::query("SELECT COUNT(*) FROM track_open WHERE id_list='{$_GET["id_data_list"]}' && (date_open BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $opens=$row[0];
        }
        
        echo $opens;
    }
   
    function get_data_list_clicks(){
        $clicks="-";
        $query = bd::query("SELECT id_news FROM data_list WHERE id='{$_GET["id_data_list"]}'");
        $row=mysql_fetch_array($query);
        $id_news=$row['id_news'];
        if($_SESSION['id-news']==$id_news){
            $query=bd::query("SELECT COUNT(*) FROM track_click WHERE id_list='{$_GET["id_data_list"]}' && (date_click BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $clicks=$row[0];
        }
        echo $clicks;
    }
   
    function get_data_list_bounces(){
        $bounces="-";
        $query = bd::query("SELECT id_news FROM data_list WHERE id='{$_GET["id_data_list"]}'");
        $row=mysql_fetch_array($query);
        $id_news=$row['id_news'];
        if($_SESSION['id-news']==$id_news){
            $query=bd::query("SELECT COUNT(*) FROM track_hard_bounce WHERE id_list='{$_GET["id_data_list"]}' && (date_bounce BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $bounces=$row[0];
        }
        echo $bounces;
    }
    
    function get_data_list_earnings(){
        $earnings="-";
        $query = bd::query("SELECT id_news FROM data_list WHERE id='{$_GET["id_data_list"]}'");
        $row=mysql_fetch_array($query);
        $id_news=$row['id_news'];
        if($_SESSION['id-news']==$id_news){
            $query=bd::query("SELECT SUM(amount) FROM track_earning WHERE id_list='{$_GET["id_data_list"]}' && (date_earning BETWEEN ('{$_GET["date_from"]} 00:00:00') AND ('{$_GET["date_to"]} 23:59:59'))");
            $row=mysql_fetch_row($query);
            $earnings=$row[0];
            if($row[0]==null){
                $earnings="0";
            }
        }
        echo $earnings;
    }
    
    function get_data_list_name($id){
        $query = bd::query("SELECT name FROM data_list WHERE id='$id'");
        $row=mysql_fetch_array($query);
        echo $row['name'];
    }
   
   
   /*
   
   function get_campaign_opens_rate(){
       $opens_rate="0";
       $query=bd::query("SELECT open_tracker FROM campaign WHERE id='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_array($query);
       if($row['open_tracker']=="1"){
           $query=bd::query("SELECT COUNT(id) FROM track_open WHERE id_campaign='{$_GET["id_campaign"]}'");
           $row=mysql_fetch_row($query);
           $opens=$row[0];
           $query=bd::query("SELECT data_sent FROM campaign WHERE id='{$_GET["id_campaign"]}'");
           $row=mysql_fetch_row($query);
           $sent=$row[0];
           $opens_rate=round(($opens/$sent)*100);
           //$opens_rate.="%";
       }else{
           $opens_rate="-";
       }
       echo $opens_rate;
   }
   
   function get_campaign_clicks(){
       $clicks="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       echo $clicks;
   }
   
   function get_campaign_clicks_rate(){
       $clicks_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT data_sent FROM campaign WHERE id='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $sent=$row[0];
       $clicks_rate=round(($clicks/$sent)*100);
       $clicks_rate.="%";
       echo $clicks_rate;
   }
   
   function get_campaign_bounces(){
       $bounces="0";
       $query=bd::query("SELECT COUNT(id) FROM track_hard_bounce WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $bounces=$row[0];
       echo $bounces;
   }
   
   function get_campaign_bounces_rate(){
       $bounces_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_hard_bounce WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $bounces=$row[0];
       $query=bd::query("SELECT data_sent FROM campaign WHERE id='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $sent=$row[0];
       $bounces_rate=round(($bounces/$sent)*100);
       $bounces_rate.="%";
       echo $bounces_rate;
   }
   
   function get_campaign_clicks_offerpage(){
       $clicks_offerpage="0";
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[LandingPage]'");
       $row=mysql_fetch_row($query);
       $clicks_offerpage=$row[0];
       echo $clicks_offerpage;
   }
   
   function get_campaign_clicks_offerpage_rate(){
       $clicks_offerpage_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[LandingPage]'");
       $row=mysql_fetch_row($query);
       $clicks_offerpage=$row[0];
       $clicks_offerpage_rate=round(($clicks_offerpage/$clicks)*100);
       $clicks_offerpage_rate.="%";
       echo $clicks_offerpage_rate;
   }
   
   function get_campaign_clicks_offerunsub(){
       $clicks_offerunsub="0";
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[Unsubscribe]'");
       $row=mysql_fetch_row($query);
       $clicks_offerunsub=$row[0];
       echo $clicks_offerunsub;
   }
   
   function get_campaign_clicks_offerunsub_rate(){
       $clicks_offerunsub_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[Unsubscribe]'");
       $row=mysql_fetch_row($query);
       $clicks_offerunsub=$row[0];
       $clicks_offerunsub_rate=round(($clicks_offerunsub/$clicks)*100);
       $clicks_offerunsub_rate.="%";
       echo $clicks_offerunsub_rate;
   }
   
   function get_campaign_clicks_serverunsub(){
       $clicks_serverunsub="0";
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[UnsubServer]'");
       $row=mysql_fetch_row($query);
       $clicks_serverunsub=$row[0];
       echo $clicks_serverunsub;
   }
   
   function get_campaign_clicks_serverunsub_rate(){
       $clicks_serverunsub_rate="0";
       $query=bd::query("SELECT COUNT(id) FROM track_click WHERE id_campaign='{$_GET["id_campaign"]}'");
       $row=mysql_fetch_row($query);
       $clicks=$row[0];
       $query=bd::query("SELECT COUNT(track_click.id) as total FROM track_click,redirect WHERE track_click.id_campaign='{$_GET["id_campaign"]}' && redirect.id=track_click.id_redirect && redirect.tag='[UnsubServer]'");
       $row=mysql_fetch_row($query);
       $clicks_serverunsub=$row[0];
       $clicks_serverunsub_rate=round(($clicks_serverunsub/$clicks)*100);
       $clicks_serverunsub_rate.="%";
       echo $clicks_serverunsub_rate;
   }
   */
   