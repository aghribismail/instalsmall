$(document).ready(function(){
    razmessage();
});

function razmessage(){
  $("#message-warning").hide();
  $("#message-warning-custom").hide();
  $("#message-error").hide();
  $("#message-success").hide();
}

function close_message_warning(){
    $("#message-warning").slideUp("slow");
}

function close_message_warning_custom(){
    $("#message-warning-custom").slideUp("slow");
}

function close_message_error(){
    $("#message-error").slideUp("slow");
}

function close_message_success(){
    $("#message-success").slideUp("slow");
}

function processing_show(){
    $("#processing").html("<div class='progress progress-info progress-striped active'><div class='bar' style='width: 100%;'>&nbsp;&nbsp;Processing...&nbsp;&nbsp;</div></div>");
    $("#processing").show();
}

function processing_hide(){
    $("#processing").hide();
}

function refresh(){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=show',
        type: 'get',
        data: {
            id_news: document.getElementById("news").value
        },
        success: function(data) {
            var list = JSON.parse(data);
            var activeon='';
            var activeoff='';
            $('#show').dataTable().fnClearTable();
            for (var i = 0; i < list.length; i++) {
                if(list[i].active==1){
                    activeon="btn-success disabled";
                    activeoff="btn-default";
                }else{
                    activeon="btn-default";
                    activeoff="btn-warning disabled";
                }
                $('#show').dataTable().fnAddData([ 
                    list[i].id,
                    list[i].name,
                    lisibilite_nombre(list[i].count),
                    "<div class='btn-group btn-toggle'><button id='active-on-"+list[i].id+"' class='btn btn-xs "+activeon+"' onclick='active_on("+list[i].id+");'>ON</button><button id='active-off-"+list[i].id+"' class='btn btn-xs "+activeoff+"'' onclick='active_off("+list[i].id+");'>OFF</button></div>",
                    "<i class='icon-signal icon-white' title='Stats' style='cursor: pointer;' onclick="+"location.href='stats/show.php?id_list="+list[i].id+"'"+"></i>" 
                ]);
            }
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
        }
    });
}


function active_on(id){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=set_active_on',
        type: 'get',
        data: {
            id: id
        },
        success: function(data) {
            $("#active-off-"+id).removeClass("btn-warning disabled").addClass("btn-default");
            $("#active-on-"+id).addClass("btn-success disabled");
            $("#message-success").slideDown("slow");
            setTimeout(function(){close_message_success();},3000);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
        }
    });
    
}

function active_off(id){
    processing_show();
    $.ajax({
        url: 'scripts.php?action=set_active_off',
        type: 'get',
        data: {
            id: id
        },
        success: function(data) {
            $("#active-on-"+id).removeClass("btn-success disabled").addClass("btn-default");
            $("#active-off-"+id).addClass("btn-warning disabled");
            $("#message-success").slideDown("slow");
            setTimeout(function(){close_message_success();},3000);
            processing_hide();
        },
        error: function() {
            $("#message-error").slideDown("slow");
            setTimeout(function(){close_message_error();},3000);
            processing_hide();
        }
    });
}

function show_news(){
    html= "<option value='0' disabled=''>Waiting for extracting News...</option>";
    $("#news").html(html);
    $.ajax({
        url: 'scripts.php?action=get_news',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#news").html(html);
        }
    });
}

function select_news_2(id){
    html= "<option value='0' disabled=''>Waiting for extracting News...</option>";
    $("#news").html(html);
    $.ajax({
        url: 'scripts.php?action=get_news',
        type: 'get',
        success: function(data) {
            var list = JSON.parse(data);
            var html = "<option value='0'>Select here...</option>";
            for (var i = 0; i < list.length; i++) {
                html += "<option value='"+list[i].id+"'>"+list[i].name+"</option>";
            }
            $("#news").html(html);
            $('#news option[value=' + id + ']').attr('selected', 'selected');
            refresh();
        }
    });
}

function lisibilite_nombre(nbr){
    var nombre = ''+nbr;
    var retour = '';
    var count=0;
    for(var i=nombre.length-1 ; i>=0 ; i--){
            if(count!=0 && count % 3 == 0)
                    retour = nombre[i]+' '+retour ;
            else
                    retour = nombre[i]+retour ;
            count++;
    }
    return retour;
}


